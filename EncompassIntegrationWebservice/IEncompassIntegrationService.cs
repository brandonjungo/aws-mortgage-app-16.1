﻿using EncompassIntegrationUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace EncompassIntegrationWebservice
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IEncompassIntegrationService" in both code and config file together.
    [ServiceContract(Namespace = Constants.Namespace)]
    public interface IEncompassIntegrationService
    {

        [OperationContract]
        string importLoans(string serverUri, string username, string password, string folderNames, string milestones, string lastModifiedDate,
                                    string sfdcUsername, string sfdcPassword, string loanOfficers);

        [OperationContract]
        string importContacts(string serverUri, string username, string password, string folderNames, string milestones, string lastModifiedDate,
                                    string sfdcUsername, string sfdcPassword, string loanOfficers);

        [OperationContract]
        string verifyProcessStatus(string sfdcUsername, string sfdcPassword, string jobId, string objectType);

        [OperationContract]
        string getFolderNames(string serverUri, string username, string password);

        [OperationContract]
        string verifyCredentials(string serverUri, string username, string password);

        [OperationContract]
        string insertContact(string serverUri, string username, string password, string contact, string folder);

        [OperationContract]
        string getMilestoneNames(string serverUri, string username, string password);
    }
}
