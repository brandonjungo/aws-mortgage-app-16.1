﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using EncompassIntegrationBusinessLogic;
using EncompassIntegrationUtils;

namespace EncompassIntegrationWebservice
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "EncompassIntegrationService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select EncompassIntegrationService.svc or EncompassIntegrationService.svc.cs at the Solution Explorer and start debugging.
    [ServiceBehavior(Namespace = Constants.Namespace)]
    public class EncompassIntegrationService : IEncompassIntegrationService
    {
        /*
         * This method retrieves loan data from Encompass
         *
         * @param serverUri location of the Encompass host
         * @param username username to connect to Encompass
         * @param password password to connect to encompas
         * @param folderNames list of folders name used to filter loans
         * @param milestones list of loan status used to filter loans
         * @param lastModifiedDate number used to create a date range to filter loans
         * @param loanOfficer list of loan officers used to filter loans
         * @param sfdcUsername usernmae to connect to Salesforce to import loans using the BULK API
         * @param sfdcPassword password to connect to Salesforce to import loans using the BULK API
         * @return the list of loans in jSON format
         */
        public string importLoans(string serverUri, string username, string password, string folderNames, string milestones, string lastModifiedDate, string sfdcUsername, string sfdcPassword, string loanOfficers)
        {
            Logic logic = new Logic();
            
            return logic.importLoans(serverUri, username, password, folderNames, milestones, lastModifiedDate, sfdcUsername, sfdcPassword, loanOfficers);
           // return logic.importLoans("https://BE11139166.ea.elliemae.net$BE11139166", "motivity", "M0tivity120", folderNames, milestones, lastModifiedDate, "juls@mpc-test.com", "angelic22", loanOfficers);
        }

        /*
         * This method gets the contact information from Encompass
         *
         * @param serverUri location of the Encompass host
         * @param username username to connect to Encompass
         * @param password password to connect to encompas
         * @param folderNames list of folders name used to filter loans
         * @param milestones list of loan status used to filter loans
         * @param lastModifiedDate number used to create a date range to filter loans
         * @param loanOfficer list of loan officers used to filter loans
         * @param sfdcUsername usernmae to connect to Salesforce to import loans using the BULK API
         * @param sfdcPassword password to connect to Salesforce to import loans using the BULK API
         * @return the list of contacts in jSON format
         */
        public string importContacts(string serverUri, string username, string password, string folderNames, string milestones, string lastModifiedDate, string sfdcUsername, string sfdcPassword, string loanOfficers)
        {
            Logic logic = new Logic();

            return logic.importContacts(serverUri, username, password, folderNames, milestones, lastModifiedDate, sfdcUsername, sfdcPassword, loanOfficers);//
            //return logic.importContacts("https://BE11139166.ea.elliemae.net$BE11139166", "motivity", "M0tivity120", folderNames, milestones, lastModifiedDate, "juls@mpc-test.com", "angelic22", loanOfficers);
        }

        public string verifyProcessStatus(string sfdcUsername, string sfdcPassword, string jobId, string objectType)
        {
            Logic logic = new Logic();
            return logic.checkJobStatus(sfdcUsername, sfdcPassword, jobId, objectType);
            //return logic.checkJobStatus("juls@mpc-test.com", "angelic22", jobId, objectType);
        }

        /*
         * This method retrieves the name of the folders available in Encompass to store Loans
         *
         * @param serverUri location of the Encompass host
         * @param username username to connect to Encompass
         * @param password password to connect to encompas
         * @return the list of folders in jSON format
         */
        public string getFolderNames(string serverUri, string username, string password)
        {
            Logic logic = new Logic();
            return logic.getFolderNames(serverUri, username, password);
            //return logic.getFolderNames("https://BE11139166.ea.elliemae.net$BE11139166", "motivity", "M0tivity120");
        }

        /*
         * This method checks if the Encompass credential are ok. 
         *
         * @param serverUri location of the Encompass host
         * @param username username to connect to Encompass
         * @param password password to connect to encompas
         * @return the list of folders in jSON format
         */
        public string verifyCredentials(string serverUri, string username, string password)
        {
            Logic logic = new Logic();
            return logic.verifyCredentials(serverUri, username, password);
            //return logic.verifyCredentials("https://BE11139166.ea.elliemae.net$BE11139166", "motivity", "M0tivity120");
        }

        /*
        * This method inserts a contact in Encompass
        *
        * @param serverUri location of the Encompass host
        * @param username username to connect to Encompass
        * @param password password to connect to encompas
        * @param contact contains the information in json format
        * @return the list of contacts in jSON format
        */
        public string insertContact(string serverUri, string username, string password, string contact, string folder)
        {
            Logic logic = new Logic();
           // return logic.insertContact("https://BE11139166.ea.elliemae.net$BE11139166", "motivity", "M0tivity120", contact, folder);
            return logic.insertContact(serverUri, username, password, contact, folder);
        }

        /*
         * This method retrieves the name of the folders available in Encompass to store Loans
         *
         * @param serverUri location of the Encompass host
         * @param username username to connect to Encompass
         * @param password password to connect to encompas
         * @return the list of folders in jSON format
         */
        public string getMilestoneNames(string serverUri, string username, string password)
        {
            Logic logic = new Logic();
            return logic.getMilestones(serverUri, username, password);
           // return logic.getMilestones("https://BE11139166.ea.elliemae.net$BE11139166", "motivity", "M0tivity120");
        }
    }
}
