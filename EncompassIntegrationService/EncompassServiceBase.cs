﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EllieMae.Encompass.Client;
using EllieMae.Encompass.Runtime;
using EllieMae.Encompass.BusinessObjects.Loans;
using EllieMae.Encompass.Collections;
using EllieMae.Encompass.Query;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.IO;
using EllieMae.Encompass.Reporting;
using EllieMae.Encompass.BusinessEnums;
using EncompassIntegrationEntities;

/* This class contains methods to apply filters when fetching information from Encompass
 * @author Angelica Buffa
 * @since 2014-10
 */
namespace EncompassIntegrationService
{
    public class EncompassServiceBase
    {
        // Public instance variable
        public EncompassConnectionHandler connection { get; set; }
        public EncompassFilters filters { get; set; }

        // Constructors
        public EncompassServiceBase(string serverUri, string userId, string password, 
                                    List<string> folderNames, List<string> milestones, string lastModifiedDay, 
                                    List<string> loanOfficers)
        {
            connection = new EncompassConnectionHandler(serverUri, userId, password);
            filters = new EncompassFilters(folderNames, milestones, lastModifiedDay, loanOfficers);
        }

        public EncompassServiceBase(string serverUri, string userId, string password)
        {
            connection = new EncompassConnectionHandler(serverUri, userId, password);
        }

        /*
         * Returns the current Encompass session
         * @param -
         * @return -
         */
        public Session getEncompassSession()
        {
            Session session = null;
            if (this.connection != null && this.connection.session != null)
            {
                session = connection.session;
            }
            return session;   
        }

        /*
         * Closes the session
         * @param -
         * @return -
         */
        public void closeSession()
        {
            if (this.connection != null)
            {
                this.connection.releaseConnection();
            }
        }

        /*
         * This method returns the LoanIdentityList
         * @param -
         * @return LoanIdentityList
         */
        public LoanIdentityList getLoanIdentities()
        {
            QueryCriterion queryFilter = filters.getQueryCriterion();
            return connection.session.Loans.Query(queryFilter);            
        }

        /*Keep this code in case we need to increase the performance*/
         public LoanReportCursor getLoansReport()
        {
            StringList fieldIds = new StringList();
            fieldIds.Add("Fields.356");    // 0 Appraised value
            fieldIds.Add("Fields.fe0117"); //1 Borrower work phone
            fieldIds.Add("Fields.ms.fun");    //2 Closing Date
            fieldIds.Add("Fields.976");    //3 CLTV
            fieldIds.Add("Fields.1335");   //4 Down Payment
            fieldIds.Add("Fields.763");    //5 Est. Closing Date
            fieldIds.Add("Fields.682");    //6 First payment date
            fieldIds.Add("Fields.230");    //7 Hazard Ins. (1st TD)
            fieldIds.Add("Fields.233");    //8 HOA (1st TD)
            fieldIds.Add("Fields.688");    //9 Index %(1st TD)
            fieldIds.Add("Fields.364");    //10 Loan Number
            fieldIds.Add("Fields.1264");   //11 Lender (1s TD)
            fieldIds.Add("Fields.2");      //12 Loan Amount (1st TD)
            fieldIds.Add("Fields.317");    //13 Loan Officer
            fieldIds.Add("Fields.362");    //14 Loan Processor
            fieldIds.Add("Fields.1401");   //15 Loan Program (1st TD)
            fieldIds.Add("Fields.19");     //16 Loan Purpose
            fieldIds.Add("Fields.1172");   //17 Loan Type 
            fieldIds.Add("Fields.761");    //18 Lock Date (1st TD)
            fieldIds.Add("Fields.353");    //19 LTV
            fieldIds.Add("Fields.689");    //20 mARGIN %(1ST TD)
            fieldIds.Add("Fields.5");      //21 Monthly Payment (1st TD)
            fieldIds.Add("Fields.232");    //22 Mortgage Ins. (1st TD)
            fieldIds.Add("Fields.1811");   //23 Occupancy
            fieldIds.Add("Fields.11");     //24 Property Address
            fieldIds.Add("Fields.12");     //25 Property City
            fieldIds.Add("Fields.14");     //26 Property State
            fieldIds.Add("Fields.1041");   //27 Property type
            fieldIds.Add("Fields.15");     //28 Property Postal Code
            fieldIds.Add("Fields.136");    //29 Purchase Price
            fieldIds.Add("Fields.3");      //30 Rate (1st td)
            fieldIds.Add("Fields.MS.STATUS");    //31 Milestone
            fieldIds.Add("Fields.4");      //32 Term (1st TD)
            fieldIds.Add("Fields.1405");   //33 Property Tax (1st TD)
            fieldIds.Add("Fields.4000");     //34 Borrower first name
            fieldIds.Add("Fields.37");     //35 Borrower last name
            fieldIds.Add("Fields.GUID");   //36 Transaction Id
            fieldIds.Add("Fields.1402");   //37 birthdate
            fieldIds.Add("Fields.VEND.X150");  // 38 Realtor Listing Agt Name
            fieldIds.Add("Fields.VEND.X139");  // 39 Realtor Buying Agt Name
            QueryCriterion queryFilter = filters.getQueryCriterion();
            LoanReportCursor results = connection.session.Reports.OpenReportCursor(fieldIds, queryFilter);
            return results;
        }

         /*Keep this code in case we need to increase the performance*/
         public LoanReportCursor getContactsReport()
         {
             StringList fieldIds = new StringList();
             fieldIds.Add("Fields.1402");       //0 birthdate
             fieldIds.Add("Fields.fe0104");     //1 Borrower Employer Address
             fieldIds.Add("Fields.fe0105");     //2 Borrower Employer City
             fieldIds.Add("Fields.fe0102");     //3 Borrower Employer Name
             fieldIds.Add("Fields.FE0106");     //4 Borrower Employer State
             fieldIds.Add("Fields.FE0107");     //5 Borrower Employer Zip
             fieldIds.Add("Fields.101");        //6 Borrower income
             fieldIds.Add("Fields.fe0110");     //7 Borrrower Job Title
             fieldIds.Add("Fields.65");         //8 Borrower SSN  
             fieldIds.Add("Fields.fe0116");     //9 Borrower Years In Line Of Work
             fieldIds.Add("Fields.1403");       //10 Coborrwer birthday
             fieldIds.Add("Fields.1268");       //11 Coborrower email
             fieldIds.Add("Fields.fe0204");     //12 Coborrower Employer Address    
             fieldIds.Add("Fields.fe0205");     //13 Coborrower  Employer City
             fieldIds.Add("Fields.fe0202");     //14 Coborrower Employer Name  
             fieldIds.Add("Fields.fe0206");     //15 Coborrower Employer  State
             fieldIds.Add("Fields.fe0207");     //16 Coborrower Employer Zip   
             fieldIds.Add("Fields.4004");         //17 Coborrower Fist Name        
             fieldIds.Add("Fields.110");        //18 Coborrower income   
             fieldIds.Add("Fields.fe0210");     //19 Coborrwer Job Title  
             fieldIds.Add("Fields.69");         //20 Coborrower Last Name
             fieldIds.Add("Fields.1480");       //21 Coborrower mobile  
             fieldIds.Add("Fields.97");         //22 Coborrower SSN
             fieldIds.Add("Fields.FE0217");     //23 Coborrower work phone
             fieldIds.Add("Fields.fe0216");     //24 Coborrower Years In Line Of Work 
             fieldIds.Add("Fields.1240");       //25 Email
             fieldIds.Add("Fields.4000");         //26 First name
             fieldIds.Add("Fields.37");         //27 Last name
             fieldIds.Add("Fields.FR0106");     //28 Mailing city
             fieldIds.Add("Fields.FR0107");     //29 Mailing state
             fieldIds.Add("Fields.FR0104");     //30 Mailing street
             fieldIds.Add("Fields.FR0108");     //31 Mailing zip
             fieldIds.Add("Fields.52");         //32 Marital status
             fieldIds.Add("Fields.1490");       //33 Mobile
             fieldIds.Add("Fields.66");         //34 Phone
             fieldIds.Add("Fields.317");        //35 Loan officer Name
             fieldIds.Add("Fields.364");        //35 Loan Number
             fieldIds.Add("Fields.VEND.X150");  //37 Realtor Listing Agt Name
             fieldIds.Add("Fields.VEND.X145");  //38 Realtor Listing Sellers Agt Addr
             fieldIds.Add("Fields.VEND.X146");  //39 Realtor Listing Sellers Agt City
             fieldIds.Add("Fields.VEND.X147");  //40 Realtor Listing Sellers Agt State
             fieldIds.Add("Fields.VEND.X148");  //41 Realtor Listing Sellers Agt Zip
             fieldIds.Add("Fields.VEND.X151");  //42 Realtor Listing Sellers Agt Phone
             fieldIds.Add("Fields.VEND.X152");  //43 Realtor Listing Sellers Agt Email
             fieldIds.Add("Fields.VEND.X153");  //44 Realtor Listing Sellers Agt Fax
             fieldIds.Add("Fields.VEND.X139");  //45 Realtor Buying Agt Name
             fieldIds.Add("Fields.VEND.X134");  //46 Realtor Buying Agt Addr
             fieldIds.Add("Fields.VEND.X135");  //47 Realtor Buying Agt City
             fieldIds.Add("Fields.VEND.X136");  //48 Realtor Buying Agt State
             fieldIds.Add("Fields.VEND.X137");  //49 Realtor Buying Agt Zip
             fieldIds.Add("Fields.VEND.X140");  //50 Realtor Buying Agt Phone
             fieldIds.Add("Fields.VEND.X141");  //51 Realtor Buying Agt Email
             fieldIds.Add("Fields.VEND.X142");  //52 Realtor Buying Agt Fax
             fieldIds.Add("Fields.MS.STATUS");       // 53 Status
             fieldIds.Add("Fields.2976"); //54 LeadS ource

             QueryCriterion queryFilter = filters.getQueryCriterion();
             LoanReportCursor results = connection.session.Reports.OpenReportCursor(fieldIds, queryFilter);
             return results;
         }

        /*
         * This method returns the list of available folders 
         * @param -
         * @return name of the folderes, concatenated
         */
        public string getLoanFolders()
        {
            string jsonResult = "";
            LoanFolders folders = null;
            GetFolderResult result = new GetFolderResult();

            try
            {
                folders = connection.session.Loans.Folders;
            }
            catch (Exception ex)
            {
                result.error = ex.Message;
            }
            
            if (folders != null && folders.Count > 0)
            {
                foreach (LoanFolder folder in folders){
                    Folder tmpFolder = new Folder()
                    {
                        folderLabel = folder.DisplayName,
                        folderName = folder.Name                        
                    };
                    result.folders.Add(tmpFolder);
                }
            }

            if (result.folders == null && result.folders.Count == 0)
            {
                result.error = "No folders exists in Encompass server.";
            }

            try
            {
                DataContractJsonSerializer js = new DataContractJsonSerializer(typeof(GetFolderResult));
                MemoryStream ms = new MemoryStream();
                js.WriteObject(ms, result);
                ms.Position = 0;
                StreamReader sr = new StreamReader(ms);
                jsonResult = sr.ReadToEnd();
                sr.Close();
                ms.Close();
            }
            catch (Exception ex)
            {
                jsonResult = "{\"error\":\"" + ex.Message + "\",\"folders\":[]}";
            }

            closeSession();
            return jsonResult;
             
        }

        /*
         * This method returns the list of available milestones 
         * @param -
         * @return name of the milestones, concatenated
         */
        public string getLoanMilestones()
        {
            string jsonResult = "";
            Milestones milestones = null;
            GetMilestonesResult result = new GetMilestonesResult();
            try
            {
                milestones = connection.session.Loans.Milestones;                
            }
            catch (Exception ex)
            {
                result.error = ex.Message;
            }

            if (milestones != null && milestones.Count > 0)
            {
                foreach (Milestone milestone in milestones)
                {
                    result.addMilestone(milestone.Name);
                }
            }
           
            if (result.milestones == null && result.milestones.Count == 0)
            {
                result.error = "No Milestones exists in Encompass server.";
            }

            try
            {
                DataContractJsonSerializer js = new DataContractJsonSerializer(typeof(GetMilestonesResult));
                MemoryStream ms = new MemoryStream();
                js.WriteObject(ms, result);
                ms.Position = 0;
                StreamReader sr = new StreamReader(ms);
                jsonResult = sr.ReadToEnd();
                sr.Close();
                ms.Close();
            }
            catch (Exception ex)
            {
                jsonResult = "{\"error\":\"" + ex.Message + "\",\"milestones\":[]}";
            }

            closeSession();
            return jsonResult;
        }

       
    }
}
