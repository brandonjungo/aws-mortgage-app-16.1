﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EllieMae.Encompass.Client;
using System.IO;
using System.Runtime.Serialization.Json;
using EncompassIntegrationEntities;


/* This class contains methods to handle the connection to Encompass
 * @author Angelica Buffa
 * @since 2014-10
 */
namespace EncompassIntegrationService
{
    public class EncompassConnectionHandler
    {
        // Private instance variables
        private string serverUri { get; set; }
        private string userId { get; set; }
        private string password { get; set; }
        private List<string> errors { get; set; }

        // Public instance variables
        public Session session { get; set; }

        //Constructor
        public EncompassConnectionHandler(string serverUri, string userId, string password)
        {
            this.serverUri = serverUri;
            this.userId = userId;
            this.password = password;
            this.errors = new List<string>();
            initSession();
        }

       
        /*
         * This method initialize Encompass libraries, must be invoked previouse any interaction with Encompass
         * @param -
         * @ return -
         */
        public static void initEncompassLibraries()
        {
            try
            {
                new EllieMae.Encompass.Runtime.RuntimeServices().Initialize();
            }
            catch (Exception ex)
            {               
            }
            
        }

        /*
         * This method initializes the session
         * @param -
         * @return -
         */
        private void initSession()
        {
            if (string.IsNullOrWhiteSpace(serverUri))
            {
                errors.Add("Server URL is empty");
            }

            if (string.IsNullOrWhiteSpace(userId))
            {
                errors.Add("User Id is empty");
            }

            if (string.IsNullOrWhiteSpace(password))
            {
                errors.Add("Password is empty");
            }

            if (errors.Count == 0)
            {

                if (session == null)
                {
                    session = new Session();
                    try
                    {
                        session.Start(serverUri, userId, password);
                    }
                    catch (Exception ex)
                    {
                        errors.Add(ex.Message);
                    }
                }
            }            
        }

        /* This method checks if the connection was established or not
         * @param -
         * @return -
         */
        public string getConnectionStatus()
        {
            string jsonResult;
            try
            {
                OperationResult result = new OperationResult();
                result.error = String.Join(String.Empty, errors.ToArray());
                result.success = this.errors != null && this.errors.Count == 0;
                DataContractJsonSerializer js = new DataContractJsonSerializer(typeof(OperationResult));
                MemoryStream ms = new MemoryStream();
                js.WriteObject(ms, result);
                ms.Position = 0;
                StreamReader sr = new StreamReader(ms);
                jsonResult = sr.ReadToEnd();
                sr.Close();
                ms.Close();
            }
            catch (Exception ex)
            {
                jsonResult = "{\"error\":\"" + ex.Message + "\",\"success\":false}";
            }
            return jsonResult;
        }

        /*
         * This method ends the connection
         * @param -
         * @return -
         */
        public void releaseConnection()
        {
            if (this.session != null)
            {
                this.session.End();
            }            
        }
    }
}
