﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EllieMae.Encompass.Client;
using EllieMae.Encompass.Query;
using EllieMae.Encompass.BusinessObjects.Loans;
using EllieMae.Encompass.BusinessObjects.Contacts;
using EncompassIntegrationEntities;
using EllieMae.Encompass.Collections;
using EncompassIntegrationUtils;
using EllieMae.Encompass.Reporting;

/* This class contains methods to retrieve transactions from Encompass and parse them to be inserted in Salesforce
 * @author Angelica Buffa
 * @since 2014-10
 */
namespace EncompassIntegrationService

{
    public class EncompassServiceForLoans : EncompassServiceBase
    {
        // Constructors
        public EncompassServiceForLoans(string serverUri, string userId, string password, 
                                            List<string> folderNames, List<string> milestones, string lastModifiedDay, List<string> loanOfficers) : 
                                            base(serverUri, userId, password, folderNames,milestones,lastModifiedDay, loanOfficers){}

        public EncompassServiceForLoans(string serverUri, string userId, string password) :
            base(serverUri, userId, password) { }

        /* This method returns the list of transactions that must be inserted in Salesforce
         * @Param
         * @Result - List of transactions
         */
        public List<string> getLoans()
        {
            List<string> transactionProperties = new List<string>(); ;
            if (this.getEncompassSession() != null)
            {

                LoanIdentityList loans = getLoanIdentities();
                if (loans != null && loans.Count > 0)
                {
                    foreach (LoanIdentity loanId in loans)
                    {
                        // Select the field values for the current loan
                        StringList fieldValues = getEncompassSession().Loans.SelectFields(loanId.Guid, getFields());
                        string transactionProperty = parseTransactionProperty(fieldValues);

                        transactionProperties.Add(transactionProperty);
                    }
                }
            }

            closeSession();
            return transactionProperties;
        }

        /* This  method returns the list of fields that must be opened for each transaction
         * @param -
         * @return StringList containing the field to be opened
         */
        private StringList getFields()
        {
            StringList fieldIds = new StringList();
            fieldIds.Add("356");    // 0 Appraised value
            fieldIds.Add("fe0117"); //1 Borrower work phone
            fieldIds.Add("ms.fun");    //2 Closing Date
            fieldIds.Add("976");    //3 CLTV
            fieldIds.Add("1335");   //4 Down Payment
            fieldIds.Add("763");    //5 Est. Closing Date
            fieldIds.Add("682");    //6 First payment date
            fieldIds.Add("230");    //7 Hazard Ins. (1st TD)
            fieldIds.Add("233");    //8 HOA (1st TD)
            fieldIds.Add("688");    //9 Index %(1st TD)
            fieldIds.Add("364");    //10 Loan Number
            fieldIds.Add("1264");   //11 Lender (1s TD)
            fieldIds.Add("2");      //12 Loan Amount (1st TD)
            fieldIds.Add("317");    //13 Loan Officer
            fieldIds.Add("362");    //14 Loan Processor
            fieldIds.Add("1401");   //15 Loan Program (1st TD)
            fieldIds.Add("19");     //16 Loan Purpose
            fieldIds.Add("1172");   //17 Loan Type 
            fieldIds.Add("761");    //18 Lock Date (1st TD)
            fieldIds.Add("353");    //19 LTV
            fieldIds.Add("689");    //20 mARGIN %(1ST TD)
            fieldIds.Add("5");      //21 Monthly Payment (1st TD)
            fieldIds.Add("232");    //22 Mortgage Ins. (1st TD)
            fieldIds.Add("1811");   //23 Occupancy
            fieldIds.Add("11");     //24 Property Address
            fieldIds.Add("12");     //25 Property City
            fieldIds.Add("14");     //26 Property State
            fieldIds.Add("1041");   //27 Property type
            fieldIds.Add("15");     //28 Property Postal Code
            fieldIds.Add("136");    //29 Purchase Price
            fieldIds.Add("3");      //30 Rate (1st td)
            fieldIds.Add("MS.STATUS");    //31 Milestone
            fieldIds.Add("4");      //32 Term (1st TD)
            fieldIds.Add("1405");   //33 Property Tax (1st TD)
            fieldIds.Add("36");     //34 Borrower first name
            fieldIds.Add("37");     //35 Borrower last name
            fieldIds.Add("GUID");   //36 Transaction Id
            fieldIds.Add("1402");   //37 birthdate
            fieldIds.Add("VEND.X150");  // 38 Realtor Listing Agt Name
            fieldIds.Add("VEND.X139");  // 39 Realtor Buying Agt Name
            fieldIds.Add("2025"); // 40 File Open Date
            fieldIds.Add("745"); // 41 Loan Application Date
            fieldIds.Add("762"); // 42 Lock Expiration Date
            fieldIds.Add("VEND.X117"); // 43 Buyer Attorney Name
            fieldIds.Add("VEND.X128"); // 44 Seller Attorney Name
            fieldIds.Add("1393"); // 45 LOS Status
            return fieldIds;
        }

        /* This method parses the Encompass information into a Contact
         * and returns the parsed TransactionContact
         * @param fieldValues the list of fields to be parsed
         * @return TransactionProperty
         */
        private string parseTransactionProperty(StringList fieldValues)
        {
            TransactionProperty transactionProperty = null;

            if (fieldValues != null)
            {
                transactionProperty = new TransactionProperty();
                transactionProperty.appraisedValue = Utils.parseStringToDoubleFormat(fieldValues[0]);
                transactionProperty.borrowerWorkPhone = fieldValues[1];
                transactionProperty.closingDate = Utils.convertStringToDatetimeFormat(fieldValues[2]);
                transactionProperty.cltvPercentage = Utils.parseStringToDoubleFormat(fieldValues[3]);
                transactionProperty.downPayment = Utils.parseStringToDoubleFormat(fieldValues[4]);
                transactionProperty.estimatedClosingDate = Utils.convertStringToDatetimeFormat(fieldValues[5]);
                transactionProperty.firstPaymentDate = Utils.convertStringToDatetimeFormat(fieldValues[6]);
                transactionProperty.harzadsIns = Utils.parseStringToDoubleFormat(fieldValues[7]);
                transactionProperty.hoa = Utils.parseStringToDoubleFormat(fieldValues[8]);
                transactionProperty.indexPercentage = Utils.parseStringToDoubleFormat(fieldValues[9]);
                transactionProperty.loanNumber = fieldValues[10];
                transactionProperty.lender = fieldValues[11];
                transactionProperty.loanAmount = Utils.parseStringToDoubleFormat(fieldValues[12]);
                transactionProperty.loanOfficer = fieldValues[13];
                transactionProperty.loanProcessor = fieldValues[14];
                transactionProperty.loanProgram = fieldValues[15];
                transactionProperty.loanPurpose = fieldValues[16];
                transactionProperty.loanType = fieldValues[17];
                transactionProperty.lockDate = Utils.convertStringToDatetimeFormat(fieldValues[18]);
                transactionProperty.ltvPercentage = Utils.parseStringToDoubleFormat(fieldValues[19]);
                transactionProperty.marginPercentage = Utils.parseStringToDoubleFormat(fieldValues[20]);
                transactionProperty.monthlyPayment = Utils.parseStringToDoubleFormat(fieldValues[21]);
                transactionProperty.mortgageIns = Utils.parseStringToDoubleFormat(fieldValues[22]);
                transactionProperty.Occupancy = fieldValues[23];
                transactionProperty.propertyAddress = fieldValues[24];
                transactionProperty.propertyCity = fieldValues[25];
                transactionProperty.propertyState = fieldValues[26];
                transactionProperty.propertyType = fieldValues[27];
                transactionProperty.propertyZip = fieldValues[28];
                transactionProperty.purchasePrice = Utils.parseStringToDoubleFormat(fieldValues[29]);
                transactionProperty.rate = Utils.parseStringToDoubleFormat(fieldValues[30]);
                transactionProperty.losMilestone = fieldValues[31];
                transactionProperty.term = fieldValues[32];   
                transactionProperty.propertyTax = Utils.parseStringToDoubleFormat(fieldValues[33]);
                //transactionProperty.realtorBuyingAgent = getFieldValue(loan.Fields[""];
                //transactionProperty.realtorListingAgent = getFieldValue(loan.Fields[""];           
                transactionProperty.borrowerFirstName = fieldValues[34];
                transactionProperty.borrowerLastName = fieldValues[35];
                transactionProperty.transactionPropertyId = fieldValues[10];
                transactionProperty.borrowerBirthday = Utils.convertStringToDatetimeFormat(fieldValues[37]);
                transactionProperty.name = transactionProperty.borrowerFirstName + " " + transactionProperty.borrowerLastName + " - Loan # " + transactionProperty.loanNumber;
                transactionProperty.fileOpenDate = Utils.convertStringToDatetimeFormat(fieldValues[40]);
                transactionProperty.loanApplicationDate = Utils.convertStringToDatetimeFormat(fieldValues[41]);
                transactionProperty.lockExpirationDate = Utils.convertStringToDatetimeFormat(fieldValues[42]);

                if (!String.IsNullOrWhiteSpace(transactionProperty.borrowerLastName) &&
                    !String.IsNullOrWhiteSpace(transactionProperty.borrowerBirthday) &&
                    !String.IsNullOrWhiteSpace(transactionProperty.loanOfficer) )
                {
                    transactionProperty.borrowerId = transactionProperty.borrowerLastName.Trim() + " " +
                                                 transactionProperty.borrowerBirthday.Trim() + " " +
                                                 transactionProperty.loanOfficer.Trim();
                }

                transactionProperty.realtorListingAgentName = fieldValues[38];
                transactionProperty.realtorBuyingAgentName = fieldValues[39];
                transactionProperty.buyerAttorneyName = fieldValues[43];
                transactionProperty.sellerAttoneyName = fieldValues[44];
                transactionProperty.losstatus = fieldValues[45];
            }


            return transactionProperty.ToString();

        }

        /*This method returns the list of Folders defined in Encompass
         * @param -
         * @return string containing the list of folders concatenated
         */
        public string getFolderNames()
        {            
            return getLoanFolders();
        }

        /*This method returns the list of Milestones defined in Encompass
         * @param -
         * @return string containing the list of milestone concatenated
         */
        public string getMilestones()
        {
            return getLoanMilestones();
        }
    }
}
