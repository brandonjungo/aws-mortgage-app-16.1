﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/* This class exposese methods to interact with Encompass
 * @author Angelica Buffa
 * @since 2014-10
 */
namespace EncompassIntegrationService
{
    public class EncompassServices
    {
        /*
         * This method retrieves loan data from Encompass
         *
         * @param serverUri location of the Encompass host
         * @param username username to connect to Encompass
         * @param password password to connect to encompas
         * @param folderNames list of folders name used to filter loans
         * @param milestones list of loan status used to filter loans
         * @param lastModifiedDate number used to create a date range to filter loans
         * @param loanOfficer list of loan officers used to filter loans
         * @param sessionId id of the Salesforce session
         * @return the list of loans in jSON format
         */
        public static List<string> getLoans(string serverUri, string username, string password, List<string> folderNames, 
                                            List<string> milestones, string lastModifiedDate, List<string> loanOfficers, string sessionId)
        {
            EncompassServiceForLoans service = new EncompassServiceForLoans(serverUri, username, password, folderNames, milestones, lastModifiedDate, loanOfficers);
            List<string> transactionProperties = service.getLoans();
            return transactionProperties;
        }

        /*
         * This method retrieves contact data from Encompass
         *
         * @param serverUri location of the Encompass host
         * @param username username to connect to Encompass
         * @param password password to connect to encompas
         * @param folderNames list of folders name used to filter loans
         * @param milestones list of loan status used to filter loans
         * @param lastModifiedDate number used to create a date range to filter loans
         * @param loanOfficer list of loan officers used to filter loans
         * @param sessionId id of the Salesforce session
         * @return the list of loans in jSON format
         */
        public static List<string> getContacts(string serverUri, string username, string password, List<string> folderNames,
                                                List<string> milestones, string lastModifiedDate, List<string> loanOfficers, string sessionId)
        {
            EncompassServiceForContacts service = new EncompassServiceForContacts(serverUri, username, password, folderNames, milestones, lastModifiedDate, loanOfficers);
         List<string> transactionContacts = service.getContacts();
            return transactionContacts;
        }


        /*
         * This method retrieves the name of the folders available in Encompass to store Loans
         *
         * @param serverUri location of the Encompass host
         * @param username username to connect to Encompass
         * @param password password to connect to encompas
         * @return the list of folders in jSON format
         */
        public static string getFolderNames(string serverUri, string username, string password)
        {
            EncompassServiceForLoans service = new EncompassServiceForLoans(serverUri, username, password);
            return service.getFolderNames();
        }

        /*
         * This method retrieves the name of the milestones available in Encompass
         *
         * @param serverUri location of the Encompass host
         * @param username username to connect to Encompass
         * @param password password to connect to encompas
         * @return the list of folders in jSON format
         */
        public static string getMilestones(string serverUri, string username, string password)
        {
            EncompassServiceForLoans service = new EncompassServiceForLoans(serverUri, username, password);
            return service.getMilestones();
        }

        /*
         * This method checks if the given credentials are ok
         *
         * @param serverUri location of the Encompass host
         * @param username username to connect to Encompass
         * @param password password to connect to encompas
         * @return the list of folders in jSON format
         */
        public static string verifyCredentials(string serverUri, string username, string password)
        {
            EncompassConnectionHandler connectionHandler = new EncompassConnectionHandler(serverUri, username, password);
            return connectionHandler.getConnectionStatus();
        }

        /*
        * This method inserts a new contact into Encompass
        * @param jsonString information for the new contact
        * @param serverUri location of the Encompass host
        * @param username username to connect to Encompass
        * @param password password to connect to encompas
        * @param folderName name of the folder that will contain the loan
        * @return the list of folders in jSON format
        */
        public static string insertContact(string jsonString,string serverUri, string username, string password, string folderName)
        {
            EncompassServiceForContacts service = new EncompassServiceForContacts(serverUri, username, password);
            return service.insertContact(jsonString, folderName);
        }
    }
}
