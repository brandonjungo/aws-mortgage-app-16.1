﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EncompassIntegrationEntities;
using EllieMae.Encompass.Collections;
using EllieMae.Encompass.Query;
using EllieMae.Encompass.BusinessObjects.Loans;
using EncompassIntegrationUtils;
using EllieMae.Encompass.BusinessObjects.Contacts;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Web.Script.Serialization;

/* This class contains methods to retrieve contacts from Encompass and parse them to be inserted in Salesforce
 * @author Angelica Buffa
 * @since 2014-10
 */
namespace EncompassIntegrationService
{
    class EncompassServiceForContacts: EncompassServiceBase
    {
        // Constructors
        public EncompassServiceForContacts(string serverUri, string userId, string password, 
                                            List<string> folderNames, List<string> milestones, string lastModifiedDay, List<string> loanOfficers) : 
                                            base(serverUri, userId, password, folderNames,milestones,lastModifiedDay, loanOfficers){}

        public EncompassServiceForContacts(string serverUri, string userId, string password) :
            base(serverUri, userId, password) { }

        /* This  method returns the list of fields that must be opened for each transaction
         * @param -
         * @return StringList containing the field to be opened
         */
        private StringList getFields()
        {
            StringList fieldIds = new StringList();
            fieldIds.Add("1402");       //0 birthdate
            fieldIds.Add("fe0104");     //1 Borrower Employer Address
            fieldIds.Add("fe0105");     //2 Borrower Employer City
            fieldIds.Add("fe0102");     //3 Borrower Employer Name
            fieldIds.Add("FE0106");     //4 Borrower Employer State
            fieldIds.Add("FE0107");     //5 Borrower Employer Zip
            fieldIds.Add("101");        //6 Borrower income
            fieldIds.Add("fe0110");     //7 Borrrower Job Title
            fieldIds.Add("65");         //8 Borrower SSN  
            fieldIds.Add("fe0116");     //9 Borrower Years In Line Of Work
            fieldIds.Add("1403");       //10 Coborrwer birthday
            fieldIds.Add("1268");       //11 Coborrower email
            fieldIds.Add("fe0204");     //12 Coborrower Employer Address    
            fieldIds.Add("fe0205");     //13 Coborrower  Employer City
            fieldIds.Add("fe0202");     //14 Coborrower Employer Name  
            fieldIds.Add("fe0206");     //15 Coborrower Employer  State
            fieldIds.Add("fe0207");     //16 Coborrower Employer Zip   
            fieldIds.Add("4004");         //17 Coborrower Fist Name        
            fieldIds.Add("110");        //18 Coborrower income   
            fieldIds.Add("fe0210");     //19 Coborrwer Job Title  
            fieldIds.Add("69");         //20 Coborrower Last Name
            fieldIds.Add("1480");       //21 Coborrower mobile  
            fieldIds.Add("97");         //22 Coborrower SSN
            fieldIds.Add("FE0217");     //23 Coborrower work phone
            fieldIds.Add("fe0216");     //24 Coborrower Years In Line Of Work 
            fieldIds.Add("1240");       //25 Email
            fieldIds.Add("4000");         //26 First name
            fieldIds.Add("37");         //27 Last name
            fieldIds.Add("FR0106");     //28 Mailing city
            fieldIds.Add("FR0107");     //29 Mailing state
            fieldIds.Add("FR0104");     //30 Mailing street
            fieldIds.Add("FR0108");     //31 Mailing zip
            fieldIds.Add("52");         //32 Marital status
            fieldIds.Add("1490");       //33 Mobile
            fieldIds.Add("66");         //34 Phone
            fieldIds.Add("317");        //35 Loan officer Name
            fieldIds.Add("364");        //36 Loan Number
            fieldIds.Add("VEND.X150");  //37 Realtor Listing Agt Name
            fieldIds.Add("VEND.X139");  //38 Realtor Buying Agt Name
            fieldIds.Add("MS.STATUS");  // 39 Status
            fieldIds.Add("2976");       // 40 LeadS ource
            fieldIds.Add("66"); // 41 HomePhone
            fieldIds.Add("AUS.X93"); // 42 Borrower Credit Score 1
            fieldIds.Add("AUS.X94"); // 43 Borrower Credit Score 2
            fieldIds.Add("AUS.X95"); // 44 Borrower Credit Score 3
            fieldIds.Add("AUS.X97"); // 45 Coborrower Credit Score 1
            fieldIds.Add("AUS.X98"); // 46 Coborrower Credit Score 2
            fieldIds.Add("AUS.X99"); // 47 Coborrower Credit Score 3
            fieldIds.Add("BE0115"); // 48 Borrower Employment Status
            fieldIds.Add("BE0215"); // 49 Coborrower Employment Status
            fieldIds.Add("BE0113"); // 50 Borrower Years on Job
            fieldIds.Add("BE0213"); // 51 Coborrowerr Years on Job
            return fieldIds;
        }

        /* This method parses the Encompass information into a Contact
         * and returns the parsed TransactionContact
         * @param fieldValues the list of fields to be parsed
         * @return TransactionContact
         */
        private TransactionContact parseBorrower(StringList fieldValues)
        {
            TransactionContact transactionContact = null;
            if (fieldValues != null)
            {
                // Verify that last name, birthdate, and loan officer is not null
                if (!String.IsNullOrWhiteSpace(fieldValues[27]) &&
                   !String.IsNullOrWhiteSpace(fieldValues[0]) &&
                   !String.IsNullOrWhiteSpace(fieldValues[35]))
                {
                    transactionContact = new TransactionContact();
                    transactionContact.birthdate = Utils.convertStringToDatetimeFormat(fieldValues[0]);
                    transactionContact.lastName = fieldValues[27];
                    transactionContact.loanOfficer = fieldValues[35];                    
                }

                if (transactionContact != null)
                {
                    transactionContact.losMilestone = fieldValues[39];
                    transactionContact.contactId = transactionContact.lastName + " " +
                                               transactionContact.birthdate + " " +
                                               transactionContact.loanOfficer;
                    transactionContact.borrowerEmployerAddress = fieldValues[1];
                    transactionContact.borrowerEmployerCityStateZip = fieldValues[2] + " " + fieldValues[4] + " " + fieldValues[5];
                    transactionContact.borrowerEmployerName = fieldValues[3];
                    transactionContact.borrowerIncome = Utils.parseStringToDoubleFormat(fieldValues[6]);
                    transactionContact.borrowerJobTitle = fieldValues[7];
                    transactionContact.borrowerSocialSecurityNumber = fieldValues[8];
                    transactionContact.borrowerYearsInLineOfWork = Utils.parseStringToDoubleFormat(fieldValues[9]);
                    transactionContact.coBorrowerBirthdate = Utils.convertStringToDatetimeFormat(fieldValues[10]);
                    transactionContact.coBorrowerEmail = fieldValues[11];
                    transactionContact.coBorrowerEmployerAddress = fieldValues[12];
                    transactionContact.coBorrowerEmployerCityStateZip = fieldValues[13] + " " + fieldValues[15] + " " + fieldValues[16];
                    transactionContact.coBorrowerEmployerName = fieldValues[14];
                    transactionContact.coBorrowerFirstName = fieldValues[17];
                    transactionContact.coBorrowerIncome = Utils.parseStringToDoubleFormat(fieldValues[18]);
                    transactionContact.coBorrowerJobTitle = fieldValues[19];
                    transactionContact.coBorrowerLastName = fieldValues[20];
                    transactionContact.coBorrowerMobile = fieldValues[21];
                    transactionContact.coBorrowerSocialSecurityNumber = fieldValues[22];
                    transactionContact.coBorrowerWorkPhone = fieldValues[23];
                    transactionContact.coBorrowerYearsInLineOfWork = Utils.parseStringToDoubleFormat(fieldValues[24]);
                    transactionContact.email = fieldValues[25];
                    transactionContact.firstName = fieldValues[26];                    
                    transactionContact.mailingCity = fieldValues[28];
                    transactionContact.mailingState = fieldValues[29];
                    transactionContact.mailingStreet = fieldValues[30];
                    transactionContact.mailingZip = fieldValues[31];
                    transactionContact.maritalStatus = fieldValues[32];
                    transactionContact.mobile = fieldValues[33];
                 //   transactionContact.phone = fieldValues[34];
                    transactionContact.leadSource = fieldValues[40];
                    transactionContact.homePhone = fieldValues[41];
                    transactionContact.borrowerEmploymentStatus = getEmploymentStatus(fieldValues[48]);
                    transactionContact.coborrowerEmploymentStatus = getEmploymentStatus(fieldValues[49]);
                    transactionContact.borrowerYearsOnJob = fieldValues[50];
                    transactionContact.coBorrowerYearsOnJob = fieldValues[51];
                    transactionContact.borrowerMiddleCreditScore = this.getMiddleScore(Utils.parseStringToDouble(fieldValues[42]),
                                                                                       Utils.parseStringToDouble(fieldValues[43]),
                                                                                       Utils.parseStringToDouble(fieldValues[44]));
                    transactionContact.coborrowerMiddleCreditScore = this.getMiddleScore(Utils.parseStringToDouble(fieldValues[45]),
                                                                                       Utils.parseStringToDouble(fieldValues[46]),
                                                                                       Utils.parseStringToDouble(fieldValues[47]));
                    
                } 
            }

            return transactionContact;
        }


        /* This method inserts a contact into Encompass
         * @param jsonString information to be inserted in json format
         * @param loanFolder name of the folder that must contain the transaction
         * @retrurn jsonResult
         */
        public string insertContact(string jsonString, string loanFolder)
        {
            string jsonResult = "";
            OperationResult result = new OperationResult();
            //jsonString ="{\"phone\":\"22033645\",\"mobile\":\"099908968\",\"maritalStatus\":\"Married\",\"mailingZip\":\"11800\",\"mailingStreet\":\"Enrique Martinez 1545\",\"mailingState\":\"Montevideo\",\"mailingCity\":\"Montevideo\",\"losSource\":\"Encompass\",\"losMilestone\":\"Lead\",\"loanOfficer\":null,\"lastName\":\"Buffa\",\"firstName\":\"Angelica\",\"fax\":\"1231231234\",\"email\":\"angelica@roundfuison.com\",\"contactId\":\"Buffa 01-12-2015 null\",\"coBorrowerYearsInLineOfWork\":\"4.00\",\"coBorrowerWorkPhone\":\"555555555\",\"coBorrowerSocialSecurityNumber\":null,\"coBorrowerMobile\":\"999999999\",\"coBorrowerLastName\":\"last name co borrower\",\"coBorrowerJobTitle\":\"Job title co borrower\",\"coBorrowerIncome\":\"2.00\",\"coBorrowerFirstName\":\"first name co borrower\",\"coBorrowerEmployerName\":\"Employer name co borrower\",\"coBorrowerEmployerCityStateZip\":\"Employer city state co borrower\",\"coBorrowerEmployerAddress\":\"Employer Address Coborrower\",\"coBorrowerEmail\":\"email@coborrower.com\",\"coBorrowerBirthdate\":\"01-13-2015\",\"borrowerYearsInLineOfWork\":\"5.00\",\"borrowerSocialSecurityNumber\":null,\"borrowerJobTitle\":\"Job title borrower\",\"borrowerIncome\":\"1.00\",\"borrowerEmployerName\":\"Employer name borrower\",\"borrowerEmployerCityStateZip\":\"Employer city/state/zip borrower\",\"borrowerEmployerAddress\":\"Employer Address Borrower\",\"birthdate\":\"01-12-2015\"}";
            if (!String.IsNullOrWhiteSpace(jsonString))
            {
                try
                {
                    var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                    TransactionContact transactionContact = serializer.Deserialize<TransactionContact>(jsonString);
                    // Create the empty shell for the new loan. At this point,
                    // the loan has not been saved to the Encompass server.
                    Loan loan = this.getEncompassSession().Loans.CreateNew();

                    // Set the loan folder and loan name for the loan
                    loan.LoanFolder = loanFolder;

                    // Set the borrower's name and property address
                    loan.Fields["1402"].Value = transactionContact.birthdate; //0 birthdate
                    loan.Fields["fe0104"].Value = transactionContact.borrowerEmployerAddress; //1 Borrower Employer Address
                    loan.Fields["fe0105"].Value = transactionContact.borrowerEmployerCityStateZip;  //2 Borrower Employer City    State Zip
                    loan.Fields["fe0102"].Value = transactionContact.borrowerEmployerName;//3 Borrower Employer Name
                    loan.Fields["101"].Value = transactionContact.borrowerIncome;//6 Borrower income
                    loan.Fields["fe0110"].Value = transactionContact.borrowerJobTitle; //7 Borrrower Job Title
                    loan.Fields["fe0116"].Value = transactionContact.borrowerYearsInLineOfWork; //9 Borrower Years In Line Of Work
                    loan.Fields["1403"].Value = transactionContact.coBorrowerBirthdate; //10 Coborrwer birthday
                    loan.Fields["1268"].Value = transactionContact.coBorrowerEmail; //11 Coborrower email
                    loan.Fields["fe0204"].Value = transactionContact.coBorrowerEmployerAddress;//12 Coborrower Employer Address    
                    loan.Fields["fe0205"].Value = transactionContact.coBorrowerEmployerCityStateZip; //13 Coborrower  Employer City
                    loan.Fields["fe0202"].Value = transactionContact.coBorrowerEmployerName; //14 Coborrower Employer Name  
                    loan.Fields["68"].Value = transactionContact.coBorrowerFirstName; //17 Coborrower First Name        
                    loan.Fields["110"].Value = transactionContact.coBorrowerIncome; //18 Coborrower income   
                    loan.Fields["fe0210"].Value = transactionContact.coBorrowerJobTitle; //19 Coborrwer Job Title  
                    loan.Fields["69"].Value = transactionContact.coBorrowerLastName; //20 Coborrower Last Name
                    loan.Fields["1480"].Value = transactionContact.coBorrowerMobile; //21 Coborrower mobile  
                    loan.Fields["FE0217"].Value = transactionContact.coBorrowerWorkPhone; //23 Coborrower work phone
                    //loan.Fields["fe0216"].Value = transactionContact.coBorrowerYearsInLineOfWork; //24 Coborrower Years In Line Of Work 
                    loan.Fields["1240"].Value = transactionContact.email;//25 Email
                    loan.Fields["36"].Value = transactionContact.firstName; //26 First name
                    loan.Fields["37"].Value = transactionContact.lastName; //27 Last name
                    loan.Fields["FR0106"].Value = transactionContact.mailingCity; //28 Mailing city
                    loan.Fields["FR0107"].Value = transactionContact.mailingState; //29 Mailing state
                    loan.Fields["FR0104"].Value = transactionContact.mailingStreet; //30 Mailing street
                    loan.Fields["FR0108"].Value = transactionContact.mailingZip; //31 Mailing zip
                    loan.Fields["52"].Value = transactionContact.maritalStatus; //32 Marital status
                    loan.Fields["1490"].Value = transactionContact.mobile;//33 Mobile
                    loan.Fields["66"].Value = transactionContact.phone; //34 Phone
                    loan.Fields["317"].Value = transactionContact.loanOfficer;//35 Loan officer Name
                    loan.Fields["66"].Value = transactionContact.homePhone;
                    //loan.Fields["BE0115"].Value = transactionContact.borrowerEmploymentStatus;
                    //loan.Fields["BE0215"].Value = transactionContact.coborrowerEmploymentStatus;
                    //loan.Fields["BE0113"].Value = transactionContact.borrowerYearsOnJob;
                    //loan.Fields["BE0213"].Value = transactionContact.coBorrowerYearsOnJob; 
                    loan.Commit();
                    loan.ForceUnlock();

                    result.error = "";
                    result.success = true;
                }
                catch (Exception ex)
                {
                    result.error = ex.Message;
                    result.success = false;
                }

                try
                {
                    jsonResult = new JavaScriptSerializer().Serialize(result);
                }
                catch (Exception ex)
                {
                    jsonResult = "{\"error\":\"" + ex.Message + "\",\"success\":false}";
                }
            }
            return jsonResult;
        }

        /* This method returns the list of contacts that must be inserted in Salesforce
         * @Param
         * @Result - List of errors
         */
        public List<string> getContacts()
        {
            List<TransactionContact> transactionContacts = new List<TransactionContact>();
            List<string> strTransactionContacts = new List<string>(); ;
            if (this.getEncompassSession() != null)
            {
                LoanIdentityList loans = getLoanIdentities();
                if (loans != null && loans.Count > 0)
                {
                    foreach (LoanIdentity loanId in loans)
                    {
                        // Select the field values for the current loan
                        StringList fieldValues = this.getEncompassSession().Loans.SelectFields(loanId.Guid, getFields());

                        TransactionContact borrower = parseBorrower(fieldValues);
                        if (borrower != null && !transactionContacts.Contains(borrower))
                        {
                            transactionContacts.Add(borrower);
                            strTransactionContacts.Add(borrower.ToString());
                        }
                    }
                }
            }

            closeSession();
            return strTransactionContacts;
        }

        private String getMiddleScore(Double scoreOne, Double scoreTwo, Double scoreThree)
        {
            String middleScore = "0";
            List<Double> scores = new List<Double>();

            if (scoreOne > 0)
            {
                scores.Add(scoreOne);
            }

            if (scoreTwo > 0)
            {
                scores.Add(scoreTwo);
            }

            if (scoreThree > 0)
            {
                scores.Add(scoreThree);
            }

            scores.Sort();

            if (scores.Count() == 1 || scores.Count() == 2)
            {
                middleScore = scores[0].ToString();
            }

            if (scores.Count() == 3)
            {
                middleScore = scores[1].ToString();
            }

            return middleScore;
        }

        public string getEmploymentStatus(string val)
        {
            string result = "";
            if (!String.IsNullOrWhiteSpace(val)){
                if (val.ToUpper().Equals("N") || val.ToUpper().Equals("NO")){
                    result = "W2";
                }

                if (val.ToUpper().Equals("Y") || val.ToUpper().Equals("YES")){
                    result = "Self-Employed";
                }
            }

            return result;
        }
    }

}
