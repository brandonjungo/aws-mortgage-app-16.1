﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using EllieMae.Encompass.Client;
using EllieMae.Encompass.Query;

/* This class contains methods to apply filters when fetching information from Encompass
 * @author Angelica Buffa
 * @since 2014-10
 */
namespace EncompassIntegrationService
{
    public class EncompassFilters
    {
        // Private instance variables
        private QueryCriterion folderFilter { get; set; }
        private QueryCriterion statusFilter { get; set; }
        private QueryCriterion lastModifiedFilter { get; set; }
        private QueryCriterion loanOfficersFilter { get; set; }

        // Private public variables
        public List<string> folderNames { get; set; }
        public List<string> milestones { get; set; }
        public string lastModifiedDay { get; set; }
        public List<string> loanOfficers { get; set; }

        // Constructor
        public EncompassFilters(List<string> folderNames, List<string> milestones, string lastModifiedDay, List<string> loanOfficers)
        {
            this.folderNames = folderNames;
            this.milestones = milestones;
            this.lastModifiedDay = lastModifiedDay;
            this.loanOfficers = loanOfficers;
            folderFilter = null;
            statusFilter = null;
            lastModifiedFilter = null;
            loanOfficersFilter = null;
        }

        /* This method retunrs the queryCriterion
         * @param -
         * @return -
         */
        public QueryCriterion getQueryCriterion()
        {
            QueryCriterion queryFilter = null;
            if (folderNames != null && folderNames.Count > 0)
            {
                folderFilter = getStringCriteria(folderNames, "Loan.LOANFOLDER");
            }

            if (milestones != null && milestones.Count > 0)
            {
                statusFilter = getStringCriteria(milestones, "Loan.CurrentMilestoneName");
            }

            if (!String.IsNullOrWhiteSpace(lastModifiedDay))
            {
                int i = 0;
                bool result = int.TryParse(lastModifiedDay, out i); 
                lastModifiedFilter = getDateCriteria(i, "Loan.LASTMODIFIED");

            }

            if (loanOfficers != null && loanOfficers.Count > 0)
            {
                loanOfficersFilter = getStringCriteria(loanOfficers, "Loan.LoanOfficerName");
            }

            queryFilter = getQueryFilter(folderFilter, statusFilter, lastModifiedFilter);
            return queryFilter;
        }

        /* This method builds  a query criterion for a string field, applying multiple filter criteria for a given value. 
         * Similar to SQL ...WHERE FIELD IN LIST_OF_VALUES
         * @param values values to compare
         * @param field field name to fitler by
         * @return query criterion
         */
        private QueryCriterion getStringCriteria(List<string> values, string field)
        {
            QueryCriterion queryCriterion = null;
            if (values != null && values.Count > 0 )
            {

                StringFieldCriterion stringFilter = new StringFieldCriterion();
                stringFilter.FieldName = field;
                stringFilter.Value = values[0];
                queryCriterion = stringFilter;
                if (values.Count > 1){
                    for (int i = 1; i < values.Count; i++)
                    {
                        stringFilter = new StringFieldCriterion();
                        stringFilter.FieldName = field;
                        stringFilter.Value = values[i];
                        queryCriterion = queryCriterion.Or(stringFilter);
                    }                       
                }               
            }
            return queryCriterion;
        }

        /* This method builds  a query criterion for a DATE field
         * @param value value to compare
         * @param field field name to fitler by
         * @return query criterion
         */
        public QueryCriterion getDateCriteria(int value, string field)
        {

            value = value * (-1);
            DateFieldCriterion dateFilter = new DateFieldCriterion();
            dateFilter.FieldName = field;
            dateFilter.Value = DateTime.Now.AddDays(value);
            dateFilter.MatchType = OrdinalFieldMatchType.GreaterThanOrEquals;

            return dateFilter;
        }

        /* This method builds a complex query criterion joining servarl filters
         * @param values values to compare
         * @param field field name to fitler by
         * @return query criterion
         */
        private QueryCriterion getQueryFilter(QueryCriterion folderFilter, QueryCriterion milestoneFilter, QueryCriterion lastModifiedFilter)
        {
            QueryCriterion queryFilter = null;

            //Keep this code in case filters become mandatory
            /*
            if (folderFilter != null && milestoneFilter != null && lastModifiedFilter != null)
            {
                queryFilter = folderFilter.And(milestoneFilter).And(lastModifiedFilter);
            }
            */
            
            if (folderFilter != null)
            {
                if (milestoneFilter != null)
                {
                    if (lastModifiedFilter != null)
                    {
                        if (loanOfficersFilter != null){
                            queryFilter = folderFilter.And(milestoneFilter).And(lastModifiedFilter).And(loanOfficersFilter);
                        }else{
                            queryFilter = folderFilter.And(milestoneFilter).And(lastModifiedFilter);
                        }                        
                    }
                    else
                    {
                        if (loanOfficersFilter != null){
                            queryFilter = folderFilter.And(milestoneFilter).And(loanOfficersFilter);
                        }else{
                            queryFilter = folderFilter.And(milestoneFilter);
                        }
                    }
                }
                else
                {
                    if (lastModifiedDay != null)
                    {
                        if (loanOfficersFilter != null){
                            queryFilter = folderFilter.And(lastModifiedFilter).And(loanOfficersFilter);
                        }else{
                            queryFilter = folderFilter.And(lastModifiedFilter);
                        }                     
                    }
                    else
                    {
                        if (loanOfficersFilter != null){
                            queryFilter = folderFilter.And(loanOfficersFilter);
                        }else{
                            queryFilter = folderFilter;
                        }                        
                    }                    
                }                
            }
            else{
                if (milestoneFilter != null)
                {
                    if (lastModifiedFilter != null)
                    {
                        if (loanOfficersFilter != null)
                        {
                            queryFilter = milestoneFilter.And(lastModifiedFilter).And(loanOfficersFilter);
                        }
                        else
                        {
                            queryFilter = milestoneFilter.And(lastModifiedFilter);
                        }
                    }
                    else
                    {
                        if (loanOfficersFilter != null)
                        {
                            queryFilter = milestoneFilter.And(loanOfficersFilter);
                        }
                        else
                        {
                            queryFilter = milestoneFilter;
                        }                        
                    }
                }
                else
                {
                    if (lastModifiedFilter != null)
                    {
                        if (loanOfficersFilter != null)
                        {
                            queryFilter = lastModifiedFilter.And(loanOfficersFilter);
                        }
                        else
                        {
                            queryFilter = lastModifiedFilter;
                        }         
                    }
                    else
                    {
                        if (loanOfficersFilter != null)
                        {
                            queryFilter = loanOfficersFilter;
                        }
                    }
                }
            }
         //   queryFilter = lastModifiedFilter;
           return queryFilter;
        }
    }
}
