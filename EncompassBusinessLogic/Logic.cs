﻿/* 
 * This class contains methods to run the integration
 *
 * @author Angelica Buffa
 * @since 2014-10
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EncompassIntegrationSalesforceService;
using EncompassIntegrationService;
using System.ComponentModel;
using System.Runtime.Remoting.Messaging;
using System.Runtime.Serialization.Json;
using System.IO;
using EncompassIntegrationEntities;

namespace EncompassIntegrationBusinessLogic
{
    public class Logic
    {
        //Private class variables
        private List<string> errors { get; set; }

        // Constructor
        public Logic()
        {
            errors = new List<string>();
        }

        /*** PRIVATE METHODS ***/
        /*
         * This method retrieves the name of the folders available in Encompass to store Loans
         *
         * @param serverUri location of the Encompass host
         * @param username username to connect to Encompass
         * @param password password to connect to encompas
         * @return the list of folders in jSON format
         */
        private string getLoanFolders(string serverUri, string username, string password)
        {
            return EncompassServices.getFolderNames(serverUri, username, password);
        }


        /*** PRIVATE METHODS ***/
        /*
         * This method retrieves the name of the folders available in Encompass to store Loans
         *
         * @param serverUri location of the Encompass host
         * @param username username to connect to Encompass
         * @param password password to connect to encompas
         * @return the list of folders in jSON format
         */
        private string getLoanMilestones(string serverUri, string username, string password)
        {
            return EncompassServices.getMilestones(serverUri, username, password);
        }

        /*
         * This method retrieves loan data from Encompass
         *
         * @param serverUri location of the Encompass host
         * @param username username to connect to Encompass
         * @param password password to connect to encompas
         * @param folderNames list of folders name used to filter loans
         * @param milestones list of loan status used to filter loans
         * @param lastModifiedDate number used to create a date range to filter loans
         * @param loanOfficer list of loan officers used to filter loans
         * @param sfdcUsername usernmae to connect to Salesforce to import loans using the BULK API
         * @param sfdcPassword password to connect to Salesforce to import loans using the BULK API
         * @return the list of loans in jSON format
         */
        private string getLoans(string serverUri, string username, string password, string folderNames,
                                string milestones, string lastModifiedDate, string sfdcUsername, string sfdcPassword,
                                string loanOfficers)
        {
            errors = new List<string>();
            string jsonResult = "";
            GetImportDataResult result = new GetImportDataResult();

            //Verify mandatory arguments
            if (String.IsNullOrWhiteSpace(serverUri))
            {
                errors.Add("Encompass host cannot be blank.");
            }

            if (String.IsNullOrWhiteSpace(username))
            {
                errors.Add("Encompass username cannot be blank.");
            }

            if (String.IsNullOrWhiteSpace(password))
            {
                errors.Add("Encompass password cannot be blank.");
            }

            if (String.IsNullOrWhiteSpace(folderNames))
            {
                errors.Add("Folder names cannot be blank.");
            }

            if (String.IsNullOrWhiteSpace(milestones))
            {
                errors.Add("Milestones cannot be blank.");
            }

            if (String.IsNullOrWhiteSpace(loanOfficers))
            {
                errors.Add("Loan Officers cannot be blank.");
            }

            if (String.IsNullOrWhiteSpace(sfdcUsername))
            {
                errors.Add("SFDC username cannot be blank.");
            }

            if (String.IsNullOrWhiteSpace(sfdcPassword))
            {
                errors.Add("SFDC password cannot be blank.");
            }

            if (errors.Count == 0)
            {
                List<string> milestoneList = milestones.Split('~').ToList();
                List<string> folderNamesList = folderNames.Split('~').ToList();
                List<string> loanOfficersList = loanOfficers.Split('~').ToList();

                // Insert the BULK API job
                SalesforceServices services = new SalesforceServices(sfdcUsername, sfdcPassword, null, null);
                result.jobId = services.createLoanJob();

                // If job was inserted, retrieve the information from Encompass
                // Retrieving the information from Encompass is time consuming.
                // Apex callouts has a max time out of 120 miliseconds. 
                // To avoid timeout issues, Encompass info will be retrieved in a asynchronus method
                if (result.jobId != null)
                {
                    // Get the Salesforce session and server URL to avoid logging in again.
                    string sessionId = services.getSalesforceSessionId();
                    string serverUrl = services.getSalesforceServerUrl();

                    // Invoke the async method to retrieve info from Encmopass. 
                    getLoansAsync(serverUri, username, password, folderNamesList, milestoneList, lastModifiedDate,
                                  result.jobId, loanOfficersList, sessionId, serverUrl);
                }
                else
                {
                    errors.Add(services.error);
                }

                if (errors.Count > 0)
                {
                    result.error = String.Join(",", errors);

                }
                try
                {
                    // Return the result ina Jason Format. 
                    DataContractJsonSerializer js = new DataContractJsonSerializer(typeof(GetImportDataResult));
                    MemoryStream ms = new MemoryStream();
                    js.WriteObject(ms, result);
                    ms.Position = 0;
                    StreamReader sr = new StreamReader(ms);
                    jsonResult = sr.ReadToEnd();
                    sr.Close();
                    ms.Close();
                }
                catch (Exception ex)
                {
                    // If any exception occurrs while serializing the jason, track the exception
                    jsonResult = "{\"error\":\"" + ex.Message + "\",\"jobId\":\"\"}";
                }
            }

            return jsonResult;
        }

        /*
         * This method gets the contact information from Encompass
         *
         * @param serverUri location of the Encompass host
         * @param username username to connect to Encompass
         * @param password password to connect to encompas
         * @param folderNames list of folders name used to filter loans
         * @param milestones list of loan status used to filter loans
         * @param lastModifiedDate number used to create a date range to filter loans
         * @param loanOfficer list of loan officers used to filter loans
         * @param sfdcUsername usernmae to connect to Salesforce to import loans using the BULK API
         * @param sfdcPassword password to connect to Salesforce to import loans using the BULK API
         * @return the list of contacts in jSON format
         */
        private string getContacts(string serverUri, string username, string password, string folderNames,
            string milestones, string lastModifiedDate, string sfdcUsername, string sfdcPassword, string loanOfficers)
        {

            errors = new List<string>();
            string jsonResult = "";
            GetImportDataResult result = new GetImportDataResult();

            //Verify mandatory arguments
            if (String.IsNullOrWhiteSpace(serverUri))
            {
                errors.Add("Encompass host cannot be blank.");
            }

            if (String.IsNullOrWhiteSpace(username))
            {
                errors.Add("Encompass username cannot be blank.");
            }

            if (String.IsNullOrWhiteSpace(password))
            {
                errors.Add("Encompass password cannot be blank.");
            }

            if (String.IsNullOrWhiteSpace(folderNames))
            {
                errors.Add("Folder names cannot be blank.");
            }

            if (String.IsNullOrWhiteSpace(milestones))
            {
                errors.Add("Milestones cannot be blank.");
            }

            if (String.IsNullOrWhiteSpace(loanOfficers))
            {
                errors.Add("Loan Officers cannot be blank.");
            }

            if (String.IsNullOrWhiteSpace(sfdcUsername))
            {
                errors.Add("SFDC username cannot be blank.");
            }

            if (String.IsNullOrWhiteSpace(sfdcPassword))
            {
                errors.Add("SFDC password cannot be blank.");
            }

            if (errors.Count == 0)
            {
                List<string> milestoneList = milestones.Split('~').ToList();
                List<string> folderNamesList = folderNames.Split('~').ToList();
                List<string> loanOfficersList = loanOfficers.Split('~').ToList();

                // Insert the BULK API job
                SalesforceServices services = new SalesforceServices(sfdcUsername, sfdcPassword, null, null);
                result.jobId = services.createContactJob();


                // If job was inserted, retrieve the information from Encompass
                // Retrieving the information from Encompass is time consuming.
                // Apex callouts has a max time out of 120 miliseconds. 
                // To avoid timeout issues, Encompass info will be retrieved in a asynchronus method
                if (result.jobId != null)
                {
                    // Get the Salesforce session and server URL to avoid logging in again.
                    string sessionId = services.getSalesforceSessionId();
                    string serverUrl = services.getSalesforceServerUrl();

                    // Invoke the async method to retrieve info from Encmopass. 
                    getContactsAsync(serverUri, username, password, folderNamesList,
                                     milestoneList, lastModifiedDate, result.jobId, 
                                     loanOfficersList, sessionId, serverUrl);
                }
                else
                {
                    errors.Add(services.error);
                }

                if (errors.Count > 0)
                {
                    result.error = String.Join(",", errors);

                }
                try
                {
                    DataContractJsonSerializer js = new DataContractJsonSerializer(typeof(GetImportDataResult));
                    MemoryStream ms = new MemoryStream();
                    js.WriteObject(ms, result);
                    ms.Position = 0;
                    StreamReader sr = new StreamReader(ms);
                    jsonResult = sr.ReadToEnd();
                    sr.Close();
                    ms.Close();
                }
                catch (Exception ex)
                {
                    jsonResult = "{\"error\":\"" + ex.Message + "\",\"jobId\":\"\"}";
                }
            }

            return jsonResult;

        }

        /*
         * This method gets the loans information from Encompass asynchronously
         *
         * @param serverUri location of the Encompass host
         * @param username username to connect to Encompass
         * @param password password to connect to encompas
         * @param folderNames list of folders name used to filter loans
         * @param milestones list of loan status used to filter loans
         * @param lastModifiedDate number used to create a date range to filter loans
         * @param loanOfficer list of loan officers used to filter loans
         * @param sessionId session id to connect to Salesforce to import loans using the BULK API
         * @param serverUrl salesforce host to connect to Salesforce to import loans using the BULK API
         * @return the list of contacts in jSON format
         */
        private void importLoansAsync(string serverUri, string username, string password, List<string> folderNames,
                                      List<string> milestones, string lastModifiedDate, string jobId,
                                      List<string> loanOfficers, string sessionId, string serverUrl)
        {
            // Get the loans from Encompass
            List<string> loans = EncompassServices.getLoans(serverUri, username, password, folderNames, milestones, lastModifiedDate, loanOfficers, sessionId);

            // Parse and import loans into Salesforce using the BULK API
            SalesforceServices services = new SalesforceServices(null, null, sessionId, serverUrl);
            if (loans != null && loans.Count > 0)
            {               
                services.importLoans(loans, jobId);
            }
            else
            {
                services.closeAbortJob(jobId);
            }
        }

        /*
        * This method gets the contact information from Encompass asynchronously
        *
        * @param serverUri location of the Encompass host
        * @param username username to connect to Encompass
        * @param password password to connect to encompas
        * @param folderNames list of folders name used to filter loans
        * @param milestones list of loan status used to filter loans
        * @param lastModifiedDate number used to create a date range to filter loans
        * @param loanOfficer list of loan officers used to filter loans
        * @param sessionId session id to connect to Salesforce to import loans using the BULK API
        * @param serverUrl salesforce host to connect to Salesforce to import loans using the BULK API
        * @return the list of contacts in jSON format
        */
        private void importContactsAsync(string serverUri, string username, string password, List<string> folderNames,
                                         List<string> milestones, string lastModifiedDate, string jobId,
                                         List<string> loanOfficers, string sessionId, string serverUrl)
        {
            // Get the contacts from Encompass
            List<string> contacts = EncompassServices.getContacts(serverUri, username, password, folderNames, milestones, lastModifiedDate, loanOfficers, sessionId);

            // Parse and import contacts into Salesforce using the BULK API
            SalesforceServices services = new SalesforceServices(null, null, sessionId, serverUrl);
            if (contacts != null && contacts.Count > 0)
            {                
                services.importContacts(contacts, jobId);
            }
            else
            {
                services.closeAbortJob(jobId);
            }
        }

        /*
        * This method verify if provided credentials are correct
        *
        * @param serverUri location of the Encompass host
        * @param username username to connect to Encompass
        * @param password password to connect to encompas
        * @return a JSON string
        */
        private string verifyEncompassCredentials(string serverUri, string username, string password)
        {
            return EncompassServices.verifyCredentials(serverUri, username, password);
        }

        /*
        * This method invokes the worker to retrieve information from encompass asynchronously
        *
        * @param serverUri location of the Encompass host
        * @param username username to connect to Encompass
        * @param password password to connect to encompas
        * @param folderNames list of folders name used to filter loans
        * @param milestones list of loan status used to filter loans
        * @param lastModifiedDate number used to create a date range to filter loans
        * @param loanOfficer list of loan officers used to filter loans
        * @param sessionId session id to connect to Salesforce to import loans using the BULK API
        * @param serverUrl salesforce host to connect to Salesforce to import loans using the BULK API
        * @return the list of contacts in jSON format
        */
        private void getLoansAsync(string serverUri, string username, string password, List<string> folderNames,
                                  List<string> milestones, string lastModifiedDate, string jobId,
                                  List<string> loanOfficers, string sessionId, string serverUrl)
        {

            ObjectWorkerDelegate worker = new ObjectWorkerDelegate(importLoansAsync);
            AsyncCallback completedCallback = new AsyncCallback(ObjectCompletedCallback);
            AsyncOperation async = AsyncOperationManager.CreateOperation(null);
            worker.BeginInvoke(serverUri, username, password, folderNames, milestones, lastModifiedDate, jobId, loanOfficers, sessionId, serverUrl, completedCallback, async);
        }

        /*
        * This method invokes the worker to retrieve information from encompass asynchronously
        *
        * @param serverUri location of the Encompass host
        * @param username username to connect to Encompass
        * @param password password to connect to encompas
        * @param folderNames list of folders name used to filter loans
        * @param milestones list of loan status used to filter loans
        * @param lastModifiedDate number used to create a date range to filter loans
        * @param loanOfficer list of loan officers used to filter loans
        * @param sessionId session id to connect to Salesforce to import loans using the BULK API
        * @param serverUrl salesforce host to connect to Salesforce to import loans using the BULK API
        * @return the list of contacts in jSON format
        */
        private void getContactsAsync(string serverUri, string username, string password, List<string> folderNames,
                                     List<string> milestones, string lastModifiedDate, string jobId,
                                     List<string> loanOfficers, string sessionId, string serverUrl)
        {

            ObjectWorkerDelegate worker = new ObjectWorkerDelegate(importContactsAsync);
            AsyncCallback completedCallback = new AsyncCallback(ObjectCompletedCallback);
            AsyncOperation async = AsyncOperationManager.CreateOperation(null);
            worker.BeginInvoke(serverUri, username, password, folderNames, milestones, lastModifiedDate, jobId, loanOfficers, sessionId, serverUrl, completedCallback, async);
        }

        /*
        * This method inserts a contact in Encompass
        *
        * @param serverUri location of the Encompass host
        * @param username username to connect to Encompass
        * @param password password to connect to encompas
        * @param contact contains the information in json format
        * @return the list of contacts in jSON format
        */
        private string insertEncompassContact(string serverUri, string username, string password, string jsonString, string folderName)
        {
            return EncompassServices.insertContact(jsonString, serverUri, username, password, folderName);
        }

        /*
        * This method retrieves folder information from Encompass
        *
        * @param serverUri location of the Encompass host
        * @param username username to connect to Encompass
        * @param password password to connect to encompas
        * @return a JSON string
        */
        public string getFolderNames(string serverUri, string username, string password)
        {
            // Init Encompass libraries. This must be called before executing any other code that interacts with Encompass
            EncompassConnectionHandler.initEncompassLibraries();

            // Any logic that interacts with Encompass must be encapsulated in a separate function, and
            // invoked right after initializing the Encompass libraries
            return getLoanFolders(serverUri, username, password);
        }

        /*
        * This method retrieves milestones information from Encompass
        *
        * @param serverUri location of the Encompass host
        * @param username username to connect to Encompass
        * @param password password to connect to encompas
        * @return a JSON string
        */
        public string getMilestones(string serverUri, string username, string password)
        {
            // Init Encompass libraries. This must be called before executing any other code that interacts with Encompass
            EncompassConnectionHandler.initEncompassLibraries();

            // Any logic that interacts with Encompass must be encapsulated in a separate function, and
            // invoked right after initializing the Encompass libraries
            return getLoanMilestones(serverUri, username, password);
        }

        /*
        * This method imports loans from Encompass into Salesforce
        *
        * @param serverUri location of the Encompass host
        * @param username username to connect to Encompass
        * @param password password to connect to encompas
        * @param folderNames list of folders name used to filter loans
        * @param milestones list of loan status used to filter loans
        * @param lastModifiedDate number used to create a date range to filter loans
        * @param loanOfficer list of loan officers used to filter loans
        * @return the list of contacts in jSON format
        */
        public string importLoans(string serverUri, string username, string password, string folderNames,
                                    string milestones, string lastModifiedDate, string sfdcUsername, 
                                    string sfdcPassword, string loanOfficers)
        {
            // Init Encompass libraries. This must be called before executing any other code that interacts with Encompass
            EncompassConnectionHandler.initEncompassLibraries();

            // Any logic that interacts with Encompass must be encapsulated in a separate function, and
            // invoked right after initializing the Encompass libraries
            return getLoans(serverUri, username, password, folderNames, milestones, lastModifiedDate, sfdcUsername, sfdcPassword, loanOfficers);
        }

        /*
        * This method imports contacts from Encompass into Salesforce
        *
        * @param serverUri location of the Encompass host
        * @param username username to connect to Encompass
        * @param password password to connect to encompas
        * @param folderNames list of folders name used to filter loans
        * @param milestones list of loan status used to filter loans
        * @param lastModifiedDate number used to create a date range to filter loans
        * @param loanOfficer list of loan officers used to filter loans
        * @return the list of contacts in jSON format
        */
        public string importContacts(string serverUri, string username, string password, string folderNames,
            string milestones, string lastModifiedDate, string sfdcUsername, string sfdcPassword, string loanOfficers)
        {
            // Init Encompass libraries. This must be called before executing any other code that interacts with Encompass
            EncompassConnectionHandler.initEncompassLibraries();

            // Any logic that interacts with Encompass must be encapsulated in a separate function, and
            // invoked right after initializing the Encompass libraries
            return getContacts(serverUri, username, password, folderNames, milestones, lastModifiedDate, sfdcUsername, sfdcPassword, loanOfficers);
        }

        /*
        * This method verifies if the Encompass credentials provided by the user are correct
        *
        * @param serverUri location of the Encompass host
        * @param username username to connect to Encompass
        * @param password password to connect to encompas
        * @return the list of contacts in jSON format
        */
        public string verifyCredentials(string serverUri, string username, string password)
        {
            // Init Encompass libraries. This must be called before executing any other code that interacts with Encompass
            EncompassConnectionHandler.initEncompassLibraries();

            // Any logic that interacts with Encompass must be encapsulated in a separate function, and
            // invoked right after initializing the Encompass libraries
            return verifyEncompassCredentials(serverUri, username, password);
        }

        /*
        * This method inserts a contact in Encompass
        *
        * @param serverUri location of the Encompass host
        * @param username username to connect to Encompass
        * @param password password to connect to encompas
        * @param contact contains the information in json format
        * @return the list of contacts in jSON format
        */
        public string insertContact(string serverUri, string username, string password, string contact, string folderName)
        {
            // Init Encompass libraries. This must be called before executing any other code that interacts with Encompass
            EncompassConnectionHandler.initEncompassLibraries();

            // Any logic that interacts with Encompass must be encapsulated in a separate function, and
            // invoked right after initializing the Encompass libraries
            return insertEncompassContact(serverUri, username, password, contact, folderName);
        }

        /*
        * This method verifies if a given BULK API job has any pending batches
        *
        * @param jobId id of the BULK API job
         * @param objectType object type 
        * @param sfdcUsername username to connect to Salesforce
        * @param sfdcPassword password to connect to Salesforce
        * @return the list of contacts in jSON format
        */
        public string checkJobStatus(string sfdcUsername, string sfdcPassword,string jobId, string objectType)
        {
            SalesforceServices services = new SalesforceServices(sfdcUsername, sfdcPassword);
            return services.verifyJobStatus(jobId, objectType);
        }

        /*** WORKERS METHODS ***/

        private delegate void ObjectWorkerDelegate(string serverUri, string username, string password, List<string> folderNames,
                                                   List<string> milestones, string lastModifiedDate,  
                                                   string jobId, List<string> loanOfficers, string sessionId, string serverUrl);
       
        private void ObjectCompletedCallback(IAsyncResult ar)
        {
            // get the original worker delegate and the AsyncOperation instance
            ObjectWorkerDelegate worker = (ObjectWorkerDelegate)((AsyncResult)ar).AsyncDelegate;
            AsyncOperation async = (AsyncOperation)ar.AsyncState;

            // finish the asynchronous operation
            worker.EndInvoke(ar);


            // raise the completed event
            AsyncCompletedEventArgs completedArgs = new AsyncCompletedEventArgs(null, false, null);
            async.PostOperationCompleted(delegate(object e) { OnLosErrorCompleted((AsyncCompletedEventArgs)e); }, completedArgs);
        }

        public event AsyncCompletedEventHandler ObjectCompleted;

        protected virtual void OnLosErrorCompleted(AsyncCompletedEventArgs e)
        {
            if (ObjectCompleted != null)
                ObjectCompleted(this, e);
        }
    }
}
