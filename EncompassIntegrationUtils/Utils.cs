﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace EncompassIntegrationUtils
{
    public class Utils
    {
        public static int getBytesCount(string val)
        {
            int result = 0;

            if (!string.IsNullOrWhiteSpace(val))
            {
                result = System.Text.ASCIIEncoding.UTF8.GetByteCount(val);
            }

            return result;
        }

        public static byte[] getBytes(string val)
        {
            byte[] result = null;

            if (!string.IsNullOrWhiteSpace(val))
            {
                result = System.Text.ASCIIEncoding.UTF8.GetBytes(val);
            }

            return result;
        }

        public static string CreateTmpFile()
        {
            string fileName = string.Empty;

            try
            {
                // Get the full name of the newly created Temporary file. 
                // Note that the GetTempFileName() method actually creates
                // a 0-byte file and returns the name of the created file.
               // fileName = Path.GetTempFileName() + ".csv";
                fileName = "testEnc.csv";

                // Craete a FileInfo object to set the file's attributes
                FileInfo fileInfo = new FileInfo(fileName);

                // Set the Attribute property of this file to Temporary. 
                // Although this is not completely necessary, the .NET Framework is able 
                // to optimize the use of Temporary files by keeping them cached in memory.
                fileInfo.Attributes = FileAttributes.Temporary;
                Console.WriteLine("TEMP file created at: " + fileName);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Unable to create TEMP file or set its attributes: " + ex.Message);
            }

            return fileName;
        }

        public static string parseValue(string val)
        {
            string result = "\"\",";

            if (!string.IsNullOrWhiteSpace(val))
            {
                result = StringToCSVCell(val);
            }

            return result;
        }

        public static string parseValue(DateTime val)
        {
            string result = "\"\",";
            Console.WriteLine("PARSE VALUE: " + val);
            if (val != null && val >= System.DateTime.Today.AddYears(-10))
            {
                result = StringToCSVCell(val.ToString("yyyy-MM-dd"));
            }
            Console.WriteLine("Resultado: " + result);
            return result;
        }

        public static string parseValue(Decimal val)
        {
            string result = "";

            try{
                result = StringToCSVCell(System.Convert.ToString(val));
            }catch(Exception ex){}

            return result;
        }

        public static string convertStringToDatetimeFormat(string val)
        {
            string result = "";

            if (!string.IsNullOrWhiteSpace(val))
            {
                DateTime dt;
                if (DateTime.TryParseExact(val, "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dt))
                {
                    result = dt.ToString("yyyy-MM-dd");

                }
            }
            return result;
        }
        public static string StringToCSVCell(string str)
        {
            if (!string.IsNullOrWhiteSpace(str))
            {
                if (str.Contains("{"))
                {
                    str = str.Replace("{", "");
                }

                if (str.Contains("}"))
                {
                    str = str.Replace("}", "");
                }
                bool mustQuote = (str.Contains(",") || str.Contains("\"") || str.Contains("\r") || str.Contains("\n"));
                if (mustQuote)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("\"");
                    foreach (char nextChar in str)
                    {
                        sb.Append(nextChar);
                        if (nextChar == '"')
                            sb.Append("\"");
                    }
                    sb.Append("\",");
                    return sb.ToString();
                }
                else
                {
                    str = "\"" + str + "\",";
                }
            }
            else
            {
                str = "\"\",";
            }           

            return str;
        }

        public static string removeLastCharacter(string val)
        {
            string result = null;

            if (!string.IsNullOrWhiteSpace(val))
            {
                result = val.Remove(val.Length - 1);
            }

            return result;
        }

        public static string parseStringToDoubleFormat(string val)
        {
            string result = "";
            Decimal number;
            if (!string.IsNullOrWhiteSpace(val))
            {
                if (Decimal.TryParse(val, out number))
                {
                    result = number.ToString();
                }

            }
            return result;
        }

        public static Double parseStringToDouble(string val)
        {
            Double number = 0;
            if (!string.IsNullOrWhiteSpace(val))
            {
                Double.TryParse(val, out number);
            }
            return number;
        }
       

    }
}
