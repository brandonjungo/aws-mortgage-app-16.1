﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EncompassIntegrationUtils
{
    public class Constants
    {
        // Ensures consistency in the namespace declarations across services
        public const string Namespace = "https://developer0030.jungointegrations.com/";
    }
}