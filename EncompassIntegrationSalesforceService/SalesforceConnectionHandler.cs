﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EncompassIntegrationSalesforceService.SalesforceReference;
using System.Net;
using System.Threading;
using System.IO;

/* 
 * This class contains methods to interact with Salesforce using the BULK API
 *
 * @author Angelica Buffa
 * @since 2014-10
 */
namespace EncompassIntegrationSalesforceService
{
    public class SalesforceConnectionHandler
    {
        // Private constants
        private const string BASE_URL = "-api.salesforce.com/services/async/32.0/job";

        // Private instance variables
        private SforceService forceService { get; set; }

        // Public instance variables
        public string sessionId { get; set; }
        public string serverUrl { get; set; }
        public string error { get; set; }

        // Constructors
        public SalesforceConnectionHandler(string username, string password, string sessionId, string serverUrl)
        {
            if (!String.IsNullOrWhiteSpace(username) && !String.IsNullOrWhiteSpace(password))
            {     
                Login(username,password);
            }
            else
            {
                if (!String.IsNullOrWhiteSpace(sessionId) && !String.IsNullOrWhiteSpace(serverUrl))
                {
                    setSession(sessionId, serverUrl);
                }
            }
            
        }

        /*
         * This method logins into Salesforce
         * @param username - Salesforce username
         * @param password - Salesforce password
         * @return -
         */
        private void Login(string username, string password)
        {
            try
            {
                forceService = new SforceService();
                forceService.SessionHeaderValue = new SessionHeader();
                LoginResult loginResult = forceService.login(username, password);
                forceService.Url = loginResult.serverUrl;
                forceService.SessionHeaderValue.sessionId = loginResult.sessionId;
                sessionId = loginResult.sessionId;
                serverUrl = loginResult.serverUrl;
            }
            catch (Exception ex)
            {
                error = ex.Message;
            }
        }

        /*
        * This method sets the session id, if it exists (to avoid login again)
        * @param sessionId - Salesforce sessionId
        * @param serverUrl - Salesforce server URL
        * @return -
        */
        private void setSession(string sessionId, string serverUrl)
        {
            try
            {
                forceService = new SforceService();
                forceService.SessionHeaderValue = new SessionHeader();
                forceService.SessionHeaderValue.sessionId = sessionId;
                forceService.Url = serverUrl;               
                this.sessionId = sessionId;
                this.serverUrl = serverUrl;
            }
            catch (Exception ex)
            {
                error = ex.Message;
            }
        }

        /*
        * This method makes the callout
        * @param endpointURL - Salesforce server URL
        * @return - stream representing the batch
        */
        private String invokeRestAPI(String endpointURL)
        {
            WebClient wc = buildWebClient();

            return wc.DownloadString(endpointURL);
        }

        /*
        * This method makes the callout without downloading the String result
        * @param endpointURL - Salesforce server URL
        * @return - stream representing the batch
        */
        private Stream invokeRestAPI2(String endpointURL)
        {
            WebClient wc = buildWebClient();

            return wc.OpenRead(endpointURL);
        }


        private String invokeRestAPI(String endpointURL, String postData)
        {
            return invokeRestAPI(endpointURL, postData, "Post", String.Empty);
        }

        private String invokeRestAPI(String endpointURL, String postData, String httpVerb, String contentType)
        {
            WebClient wc = buildWebClient();

            if (String.IsNullOrWhiteSpace(contentType) == false)
            {
                wc.Headers.Add("Content-Type: " + contentType);
            }

            try
            {
                return wc.UploadString(endpointURL, httpVerb, postData);
            }
            catch (WebException webEx)
            {
                if (webEx.Response != null)
                {
                    using (var errorResponse = (HttpWebResponse)webEx.Response)
                    {
                        using (var reader = new System.IO.StreamReader(errorResponse.GetResponseStream()))
                        {
                            error = reader.ReadToEnd();
                        }
                    }
                }

                throw;
            }
        }

        /*
        * This method builds the http request
        * @param -
        * @return - WebClient
        */
        private WebClient buildWebClient()
        {
            WebClient wc = new WebClient();
            wc.Encoding = Encoding.UTF8;
            wc.Headers.Add("X-SFDC-Session: " + sessionId);
            return wc;
        }

        /*
        * This method  builds the base URL to access the information for a job Id
        * @param jobId - id of the job
        * @return - String url
        */
        private String buildSpecificJobUrl(String jobId)
        {
            return getSfdcInstance() + BASE_URL + "/" + jobId;
        }

        /*
        * This method gets the Salesforce Endpoint
        * @param -
        * @return - String url
        */
        private String getSfdcInstance()
        {
            String podPart = "";
            if (forceService != null)
            {
                String[] urlParts = forceService.Url.Split(new char[] { '.' });
                podPart = urlParts[0];
            }

            return podPart;
        }

        /*
        * This method creates a BULK API Job request
        * @param xml representation of the request
        * @return - BulkAPIJob
        */
        public BulkAPIJob CreateJob(BulkAPICreateJobRequest createJobRequest)
        {
            String jobRequestXML =
            @"<?xml version=""1.0"" encoding=""UTF-8""?>
             <jobInfo xmlns=""http://www.force.com/2009/06/asyncapi/dataload"">
               <operation>{0}</operation>
               <object>{1}</object>
               {3}
               <contentType>{2}</contentType>
             </jobInfo>";

            String externalField = String.Empty;

            if (String.IsNullOrWhiteSpace(createJobRequest.ExternalIdFieldName) == false)
            {
                externalField = "<externalIdFieldName>" + createJobRequest.ExternalIdFieldName + "</externalIdFieldName>";
            }

            jobRequestXML = String.Format(jobRequestXML,
                                          createJobRequest.OperationString,
                                          createJobRequest.Object,
                                          createJobRequest.ContentTypeString,
                                          externalField);
            
           
            String createJobUrl = getSfdcInstance() + BASE_URL;

            String resultXML = invokeRestAPI(createJobUrl, jobRequestXML);

            return BulkAPIJob.Create(resultXML);
        }

        /*
       * This method close a BULK API Job
       * @param jobId - id of the job to be closed
       * @return - BulkAPIJob
       */
        public BulkAPIJob CloseJob(String jobId)
        {
            String closeJobUrl = buildSpecificJobUrl(jobId);
            String closeRequestXML =
            @"<?xml version=""1.0"" encoding=""UTF-8""?>" + Environment.NewLine +
            @"<jobInfo xmlns=""http://www.force.com/2009/06/asyncapi/dataload"">" + Environment.NewLine +
             "<state>Closed</state>" + Environment.NewLine +
             "</jobInfo>";

            String resultXML = invokeRestAPI(closeJobUrl, closeRequestXML);

            return BulkAPIJob.Create(resultXML);
        }

        /*
        * This method creates a batch for a given Bulk API Job
        * @param BulkAPICreateBatchRequest - xml string representation of the new batch
        * @return - BulkAPIBatch
        */
        public BulkAPIBatch CreateBatch(BulkAPICreateBatchRequest createBatchRequest)
        {
            String requestUrl = getSfdcInstance() + BASE_URL + "/" + createBatchRequest.JobId + "/batch";

            String requestXML = createBatchRequest.BatchContents;

            String contentType = String.Empty;

            if (createBatchRequest.BatchContentType.HasValue)
            {
                contentType = createBatchRequest.BatchContentHeader;
            }

            String resultXML = invokeRestAPI(requestUrl, requestXML, "Post", contentType);

            return BulkAPIBatch.CreateBatch(resultXML);
        }

        /*
        * This method returns a  Bulk API Job based on the id of a Job
        * @param jobId - job Id
        * @return - BulkAPIJob
        */
        public BulkAPIJob GetJob(String jobId)
        {
            String getJobUrl = buildSpecificJobUrl(jobId);

            String resultXML = invokeRestAPI(getJobUrl);

            return BulkAPIJob.Create(resultXML);
        }

        /*
        * This method returns the list of batches for a given BULK API Job
        * @param jobId - job Id
        * @return - list of BulkAPIBatch
        */
        public List<BulkAPIBatch> GetBatches(String jobId)
        {
            String requestUrl = getSfdcInstance() + BASE_URL + "/" + jobId + "/batch/";

            String resultXML = invokeRestAPI(requestUrl);

            return BulkAPIBatch.CreateBatches(resultXML);
        }

        /*
        * This method returns the information for a specific batch that belongs to certain jobId
        * @param jobId - job Id
        * @param batchId - batch Id
        * @return - stream representing the batch
        */
        public Stream GetBatchResult(String jobId, string batchId)
        {
            String requestUrl = getSfdcInstance() + BASE_URL + "/" + jobId + "/batch/" + batchId + "/result";

            Stream resultXML = invokeRestAPI2(requestUrl);

            return resultXML;
        }

        /*
        * This method creates a batch request for a given jobId
        * @param jobId - id of the job that will contain the batch
        * @param sb - contains the information for the new batch
        * @return - 
        */
        public void createBatchRequest(string jobId, StringBuilder sb)
        {

            if (sessionId != null)
            {
                BulkAPICreateBatchRequest batchRequest = new BulkAPICreateBatchRequest();
                batchRequest.JobId = jobId;
                batchRequest.BatchContents = sb.ToString();
                batchRequest.BatchContentType = BatchContentType.CSV;
                CreateBatch(batchRequest);
            }
        }

        /*
        * This method creates a job request
        * @param objectType - job object type
        * @param operation - job operation (insert, update, etc.)
        * @param externalIdField - external id, only used if operation is Upsert
        * @return - 
        */
        public string createJobRequest(String objectType, JobOperation operation, String externalIdField)
        {
            string jobId = null;
            BulkAPIJob job = null;
            if (sessionId != null)
            {
                BulkAPICreateJobRequest jobRequest = new BulkAPICreateJobRequest();
                jobRequest.ContentType = JobContentType.CSV;
                jobRequest.Operation = operation;
                jobRequest.Object = objectType;
                jobRequest.ExternalIdFieldName = externalIdField;
                job = CreateJob(jobRequest);
            }
            if (job != null)
            {
                jobId = job.Id;
            }
            return jobId;
        }

   
    }
}
