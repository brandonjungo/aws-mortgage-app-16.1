﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/* 
 * This class defines the structure of a BULK API Job Batch
 *
 * @author Angelica Buffa
 * @since 2014-10
 */

namespace EncompassIntegrationSalesforceService
{
    public class BulkAPICreateBatchRequest
    {
        // Public instance variables
        public String JobId { get; set; }
        public String BatchContents { get; set; }
        public BatchContentType? BatchContentType { get; set; }

        internal String BatchContentHeader
        {
            get
            {
                switch (BatchContentType)
                {
                    case EncompassIntegrationSalesforceService.BatchContentType.CSV:
                        return "text/csv";
                    case EncompassIntegrationSalesforceService.BatchContentType.XML:
                        return "application/xml";
                    default:
                        return "text/csv";
                }
            }
        }
    }

    public enum BatchContentType
    {
        CSV,
        XML
    }
}
