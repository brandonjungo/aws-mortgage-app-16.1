﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;
using EncompassIntegrationEntities;
using EncompassIntegrationUtils;
using System.ComponentModel;
using System.Runtime.Remoting.Messaging;

/* 
 * This class contains methods to interact with Salesforce
 *
 * @author Angelica Buffa
 * @since 2014-10
 */

namespace EncompassIntegrationSalesforceService
{
    public class SalesforceServices
    {
        // Private constants
        private const string PROPERTY_CSV_HEADER = "\"MTGPLANNER_CRM__APPRAISED_VALUE__c\",\"MTGPLANNER_CRM__CLOSING_DATE__C\",\"MTGPLANNER_CRM__CLTV__C\",\"MTGPLANNER_CRM__DOWN_PAYMENT__C\",\"MTGPLANNER_CRM__EST_CLOSING_DATE__C\",\"MTGPLANNER_CRM__FIRST_PAYMENT_DATE__C\",\"MTGPLANNER_CRM__HAZARD_INS_1ST_TD__C\",\"MTGPLANNER_CRM__HOA_1ST_TD__C\",\"MTGPLANNER_CRM__INDEX_1ST_TD__C\",\"MTGPLANNER_CRM__LOAN_1ST_TD__C\",\"MTGPLANNER_CRM__LENDER_1ST_TD__C\",\"MTGPLANNER_CRM__LOAN_AMOUNT_1ST_TD__C\",\"Jungo_LOS__Loan_Processor__c\",\"MTGPLANNER_CRM__LOAN_PROGRAM_1ST_TD__C\",\"MTGPLANNER_CRM__LOAN_PURPOSE__C\",\"MTGPLANNER_CRM__LOAN_TYPE_1ST_TD__C\",\"MTGPLANNER_CRM__LOCK_DATE_1ST_TD__C\",\"MTGPLANNER_CRM__LTV__C\",\"MTGPLANNER_CRM__MARGIN_1ST_TD__C\",\"MTGPLANNER_CRM__MONTHLY_PAYMENT_1ST_TD__C\",\"MTGPLANNER_CRM__MORTGAGE_INS_1ST_TD__C\",\"MTGPLANNER_CRM__OCCUPANCY__C\",\"MTGPLANNER_CRM__PROPERTY_ADDRESS__C\",\"MTGPLANNER_CRM__PROPERTY_CITY__C\",\"MTGPLANNER_CRM__PROPERTY_STATE__C\",\"MTGPLANNER_CRM__PROPERTY_TAX_1ST_TD__C\",\"MTGPLANNER_CRM__PROPERTY_TYPE__C\",\"MTGPLANNER_CRM__PROPERTY_POSTAL_CODE__C\",\"MTGPLANNER_CRM__PURCHASE_PRICE__C\",\"MTGPLANNER_CRM__RATE_1ST_TD__C\",\"MTGPLANNER_CRM__TERM_1ST_TD__C\",\"Jungo_LOS__LOS_TRANSACTION_PROPERTY_ID__C\",\"NAME\",\"MTGPLANNER_CRM__BORROWER_NAME__R.Jungo_LOS__LOS_CONTACT_ID__C\",\"Jungo_LOS__LOS_Loan_Officer__c\",\"Jungo_LOS__LOS_Source__c\",\"Jungo_LOS__LOS_Milestone__c\",\"MtgPlanner_CRM__File_Open_Date__c\",\"MtgPlanner_CRM__Loan_Application_Date__c\",\"MtgPlanner_CRM__Lock_Exp_Date_1st_TD__c\",\"Jungo_LOS__LOS_Realtor_Buying_Agent_Name__c\",\"Jungo_LOS__LOS_Realtor_Listing_Agent_Name__c\",\"Jungo_LOS__LOS_Sellers_Attorney_Name__c\",\"Jungo_LOS__LOS_Buyers_Attorney_Name__c\",\"Jungo_LOS__LOS_Status__c\"";
        private const string CONTACT_CSV_HEADER = "\"Jungo_LOS__LOS_CONTACT_ID__C\",\"BIRTHDATE\",\"MTGPLANNER_CRM__EMPLOYER_CITY_STATE_ZIP__C\",\"MTGPLANNER_CRM__EMPLOYER_NAME_BORROWER__C\",\"MTGPLANNER_CRM__INCOME_BORROWER__C\",\"MTGPLANNER_CRM__JOB_TITLE_BORROWER__C\",\"MTGPLANNER_CRM__SSN__C\",\"MTGPLANNER_CRM__YEARS_IN_LINE_OF_WORK__C\",\"MTGPLANNER_CRM__EMPLOYER_ADDRESS__C\",\"MTGPLANNER_CRM__BIRTHDAYCOBORROWER__C\",\"MTGPLANNER_CRM__CO_BORROWER_EMAIL__C\",\"MTGPLANNER_CRM__CO_BORROWER_EMPLOYER_ADDRESS__C\",\"MTGPLANNER_CRM__CO_BORROWER_EMPLOYER_CITY_STATE_ZIP__C\",\"MTGPLANNER_CRM__CO_BORROWER_EMPLOYER_NAME__C\",\"MTGPLANNER_CRM__CO_BORROWER_FIRST_NAME__C\",\"MTGPLANNER_CRM__INCOME_CO_BORROWER__C\",\"MTGPLANNER_CRM__CO_BORROWER_TITLE__C\",\"MTGPLANNER_CRM__CO_BORROWER_LAST_NAME__C\",\"MTGPLANNER_CRM__CO_BORROWER_MOBILE__C\",\"MTGPLANNER_CRM__CO_BORROWER_SSN__C\",\"MTGPLANNER_CRM__CO_BORROWER_WORK_PHONE__C\",\"MTGPLANNER_CRM__CO_BORROWER_YEARS_IN_LINE_OF_WORK__C\",\"EMAIL\",\"FIRSTNAME\",\"LASTNAME\",\"MAILINGCITY\",\"MAILINGSTATE\",\"MAILINGSTREET\",\"MAILINGPOSTALCODE\",\"MTGPLANNER_CRM__MARITAL_STATUS__C\",\"MOBILEPHONE\",\"Jungo_LOS__LOS_Loan_Officer__c\",\"Jungo_LOS__LOS_Source__c\",\"Jungo_LOS__LOS_Milestone__c\",\"Fax\",\"LeadSource\",\"HomePhone\",\"MtgPlanner_CRM__Credit_Score_Borrower__c\",\"MtgPlanner_CRM__Co_Borrower_Credit_Score__c\",\"MtgPlanner_CRM__Employment_Status__c\",\"MtgPlanner_CRM__Co_Borrower_Employment_Status__c\",\"MtgPlanner_CRM__Years_on_Job__c\",\"MtgPlanner_CRM__Co_Borrower_Years_on_Job__c\"";
        private const string TRANSACTION_OBJECT = "MtgPlanner_CRM__Transaction_Property__c";
        private const string TRANSACTION_EXTERNALID = "Jungo_LOS__LOS_Transaction_Property_Id__c";
        private const string CONTACT_OBJECT = "Contact";
        private const string CONTACT_EXTERNALID = "Jungo_LOS__LOS_Contact_Id__c";
        
        
        // Private instance variables
        private SalesforceServiceForObjects objectHandler { get; set; }
        
        // Public instance variables
        public string error { get; set; }
        
        // Constructor
        public SalesforceServices(string sfdcUsername, string sfdcPassword, string sessionId, string serverUrl)
        {            
            objectHandler = new SalesforceServiceForObjects(sfdcUsername, sfdcPassword,sessionId,serverUrl);  
        }

        public SalesforceServices(string sfdcUsername, string sfdcPassword)
        {
            objectHandler = new SalesforceServiceForObjects(sfdcUsername, sfdcPassword, null, null);
        }

        /*
         * This method creats a new Bulk Api Job to upsert MtgPlanner_CRM__Transaction_Property__c
         * @ param -
         * @ return - Id of the created job
         */
        public string createLoanJob()
        {
            string jobId = null;
            jobId = objectHandler.createBulkJob(TRANSACTION_OBJECT, JobOperation.Upsert, TRANSACTION_EXTERNALID);
            return jobId;
        }

        /*
         * This method imports a list of transactions into Salesforce
         * @ param properties - list of properties to be inserted
         * @ param jobId - Id of the Bulk Api Job
         * @ return - 
         */
        public void importLoans(List<string> properties, string jobId)
        {
            objectHandler.importObjects(properties, jobId, PROPERTY_CSV_HEADER);
        }

        /*
         * This method creats a new Bulk Api Job to upsert Contacts
         * @ param -
         * @ return - Id of the created job
         */
        public string createContactJob()
        {
            string jobId = null;
            jobId = objectHandler.createBulkJob(CONTACT_OBJECT, JobOperation.Upsert, CONTACT_EXTERNALID);
            return jobId;
        }

        /*
         * This method imports a list of contacts into Salesforce
         * @ param properties - list of properties to be inserted
         * @ param jobId - Id of the Bulk Api Job
         * @ return - 
         */
        public void importContacts(List<string> contacts, string jobId)
        {
            objectHandler.importObjects(contacts, jobId, CONTACT_CSV_HEADER);
        }

        /*
         * This method verifies if a job has batches that have not been processed
         * @ param objectType - job object type
         * @ param jobId - Id of the Bulk Api Job
         * @ return string representation of the result
         */
        public string verifyJobStatus(string jobId, string objectType)
        {
            return objectHandler.verifyJobStatus(jobId, objectType);
        }

        /*
         * This method returns the id of the current Salesforce session
         * @ param objectType - 
         * @ return session id
         */
        public string getSalesforceSessionId()
        {
            return objectHandler.getSessionId();
        }

        /*
         * This method returns the server URL of the current Salesforce session
         * @ param objectType - 
         * @ return server URL
         */
        public string getSalesforceServerUrl()
        {
            return objectHandler.getServerUrl();
        }

        /*
         * This method aborts a Bulk Api Job
         * @ param jobId - id of the job
         * @ return -
         */
        public void closeAbortJob(string jobId)
        {
            if (!String.IsNullOrWhiteSpace(jobId))
            {
                this.objectHandler.closeJob(jobId);

            }
        }
       
    }
}
