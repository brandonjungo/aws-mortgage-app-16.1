﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

/* 
 * This class defines the structure of a Bulk API Batch. It has methods to create and parse the batch information
 *
 * @author Angelica Buffa
 * @since 2014-10
 */
namespace EncompassIntegrationSalesforceService
{
    public class BulkAPIBatch
    {
        // Public instance variables
        public String Id { get; set; }
        public String JobId { get; set; }
        public String State { get; set; }
        public String StateMessage { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime SystemModStamp { get; set; }
        public int NumberRecordsProcessed { get; set; }
        public int NumberRecordsFailed { get; set; }
        public int TotalProcessingTime { get; set; }
        public int ApiActiveProcessingTime { get; set; }
        public int ApexProcessingTime { get; set; }

        /* 
         * This method parse the XML returned by Salesforce when a BULK API Job is processed, and returns the list of batches
         * @param batchesResultXML string representation of the HTTP Response returned by Salesforce. 
         * @return list of BulkAPIBatch 
         */
        public static List<BulkAPIBatch> CreateBatches(String batchesResultXML)
        {
            XDocument doc = XDocument.Parse(batchesResultXML);
            XElement batchInfoList = doc.Root;
            List<BulkAPIBatch> batches = new List<BulkAPIBatch>();

            foreach (XNode batchInfoNode in batchInfoList.Nodes())
            {
                BulkAPIBatch batch = BulkAPIBatch.CreateBatch(batchInfoNode.ToString());

                batches.Add(batch);
            }

            return batches;
        }

        /* 
         * This method parse the XML that represents a BULK API Batch
         * @param xml string representation of the batch
         * @return BulkAPIBatch 
         */
        public static BulkAPIBatch CreateBatch(string resultXML)
        {
            XDocument doc = XDocument.Parse(resultXML);
            XElement batchInfoElement = doc.Root;
            List<XElement> jobInfoChildElements = batchInfoElement.Elements().ToList();

            BulkAPIBatch batch = new BulkAPIBatch();

            foreach (XElement e in jobInfoChildElements)
            {
                switch (e.Name.LocalName)
                {
                    case "id":
                        batch.Id = e.Value;
                        break;
                    case "jobId":
                        batch.JobId = e.Value;
                        break;
                    case "createdDate":
                        batch.CreatedDate = DateTime.Parse(e.Value);
                        break;
                    case "systemModstamp":
                        batch.SystemModStamp = DateTime.Parse(e.Value);
                        break;
                    case "state":
                        batch.State = e.Value;
                        break;
                    case "stateMessage":
                        batch.StateMessage = e.Value;
                        break;
                    case "numberRecordsProcessed":
                        batch.NumberRecordsProcessed = int.Parse(e.Value);
                        break;
                    case "numberRecordsFailed":
                        batch.NumberRecordsFailed = int.Parse(e.Value);
                        break;
                    case "totalProcessingTime":
                        batch.TotalProcessingTime = int.Parse(e.Value);
                        break;
                    case "apiActiveProcessingTime":
                        batch.ApiActiveProcessingTime = int.Parse(e.Value);
                        break;
                    case "apexProcessingTime":
                        batch.ApexProcessingTime = int.Parse(e.Value);
                        break;
                }
            }

            return batch;
        }
    }
}
