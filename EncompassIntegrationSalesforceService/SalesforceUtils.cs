﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using EncompassIntegrationEntities;
using EncompassIntegrationUtils;
using System.ComponentModel;
using System.Runtime.Remoting.Messaging;
using Microsoft.VisualBasic.FileIO;
using System.Runtime.Serialization.Json;

/* 
 * This class contains methods to create the csv files that will be attached to the BULK API batches
 *
 * @author Angelica Buffa
 * @since 2014-10
 */
namespace EncompassIntegrationSalesforceService
{
    class SalesforceServiceForObjects
    {

        // Private instance variables
        private SalesforceConnectionHandler session { get; set; }

        // Constructor
        public SalesforceServiceForObjects(string username, string password, string sessionId, string serverUrl)
        {
            session = new SalesforceConnectionHandler(username, password, sessionId, serverUrl);
        }

        /*
         * This method creates a BULK API Job
         * @param objectType - BULK API Job type
         * @param operation - BULK API Job operation
         * @param externalIdField - Field to be used as external ID, only used if it is an Upsert
         * @return xml string representation of the created job
         */
        public string createBulkJob(string objectType, JobOperation operation, string externalIdField)
        {
            string jobId = null;
            if (session != null && !String.IsNullOrWhiteSpace(session.sessionId))
            {                
                jobId = session.createJobRequest(objectType, operation, externalIdField);
            }
            
            return jobId;
        }

        /*
         * This method creates the csv file to be uploaded
         * @param objects - list of objcts that must be added to the job
         * @param jobId - id of the BULK API Job
         * @param header - header of the csv
         * @return -
         */
        public void importObjects(List<string> objects, string jobId, string header)
        {
            byte[] headerBytes = Utils.getBytes(header);
            int headerBytesLength = Utils.getBytesCount(header);
            StringBuilder sb = new StringBuilder();
            int maxBytesPerBatch = 10000000; // 10 million bytes per batch
            int maxRowsPerBatch = 10000; // 10 thousand rows per batch
            int currentBytes = 0;
            int currentLines = 0;
            string line = "";

            foreach (string s in objects)
            {
                line = s + System.Environment.NewLine;
                try
                {
                    byte[] lineBytes = Utils.getBytes(line);
                    int lineBytesLength = Utils.getBytesCount(line);

                    // Create a new batch when our batch size limit is reached
                    if (currentBytes + lineBytesLength > maxBytesPerBatch || currentLines > maxRowsPerBatch)
                    {
                        session.createBatchRequest(jobId, sb);
                        sb = new StringBuilder();
                        currentBytes = 0;
                        currentLines = 0;
                    }

                    if (currentBytes == 0)
                    {
                        sb.Append(header);
                        sb.Append(System.Environment.NewLine);
                        currentBytes = headerBytesLength;
                        currentLines = 1;
                    }

                    sb.Append(line);
                    currentBytes += lineBytesLength;
                    currentLines++;
                }
                catch (Exception ex) { }
            }
            if (currentLines > 1)
            {
                session.createBatchRequest(jobId, sb);
            }
           
            closeJob(jobId);
        }

        /*
         * This method returns current Salesforce session Id
         * @param -
         * @return - Salesforce session Id
         */
        public string getSessionId()
        {
            return this.session.sessionId;
        }

        /*
         * This method returns current Salesforce server URL
         * @param -
         * @return - Salesforce server URL
         */
        public string getServerUrl()
        {
            return this.session.serverUrl;
        }

        /*
         * This method returns closes a Job
         * @param - jobId id of the bulk job
         * @return - 
         */
        public void closeJob(string jobId)
        {
            if (session != null)
            {
                session.CloseJob(jobId);
            }                
        }

        /*
        * This method checks the progress of the job
        * @param - jobId id of the bulk job
        * @param - objectType
        * @return - result
        */
        public String verifyJobStatus(string jobId, string objectType)
        {
            String jsonResult = "";
            OperationResult result = new OperationResult();
            List<string> issuedBatchIds = new List<string>();

            if (string.IsNullOrWhiteSpace(jobId) || string.IsNullOrWhiteSpace(session.sessionId))
            {
                result.success = false;
                result.error = "Job Id cannot be blank.";
            }

            if (result.success)
            {
                List<BulkAPIBatch> batches = session.GetBatches(jobId);

                foreach (BulkAPIBatch batch in batches)
                {
                    if (batch.State == "Completed" && batch.NumberRecordsFailed > 0)
                    {
                        issuedBatchIds.Add(batch.Id);
                    }

                    if (batch.State == "InProgress" || batch.State == "Queued")
                    {
                        result.success = false;
                    }
                }
            }

            if (issuedBatchIds != null && issuedBatchIds.Count > 0 && result.success)
            {
                // TODO - Send email
            }

            try
            {
                DataContractJsonSerializer js = new DataContractJsonSerializer(typeof(OperationResult));
                MemoryStream ms = new MemoryStream();
                js.WriteObject(ms, result);
                ms.Position = 0;
                StreamReader sr = new StreamReader(ms);
                jsonResult = sr.ReadToEnd();
                sr.Close();
                ms.Close();
            }
            catch (Exception ex)
            {
                jsonResult = "{\"error\":\"" + ex.Message + "\",\"success\":false}";
            }
            return jsonResult;
        }

        
    }

    
    
}
