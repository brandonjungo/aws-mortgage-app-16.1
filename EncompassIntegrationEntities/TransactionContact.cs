﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EncompassIntegrationEntities;
using EncompassIntegrationUtils;

/* 
 * This class defines the bean for the Transaction Property object. Implements IEquatable to overwrite the Equals method
 *
 * @author Angelica Buffa
 * @since 2014-10
 */
namespace EncompassIntegrationEntities
{
    public class TransactionContact : IEquatable<TransactionContact>
    {
        // Public instance variables
        public string contactId { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string birthdate { get; set; }
        public string mailingStreet { get; set; }
        public string mailingCity { get; set; }
        public string mailingState { get; set; }
        public string mailingZip { get; set; }
        public string phone { get; set; }
        public string mobile { get; set; }
        public string email { get; set; }
        public string maritalStatus { get; set; }
        public string coBorrowerFirstName { get; set; }
        public string coBorrowerLastName { get; set; }
        public string coBorrowerBirthdate { get; set; }
        public string coBorrowerEmail { get; set; }
        public string coBorrowerWorkPhone { get; set; }
        public string coBorrowerMobile { get; set; }
        public string coBorrowerEmployerName { get; set; }
        public string coBorrowerJobTitle { get; set; }
        public string coBorrowerYearsInLineOfWork { get; set; }
        public string coBorrowerEmployerAddress { get; set; }
        public string coBorrowerEmployerCityStateZip { get; set; }
        public string coBorrowerIncome { get; set; }
        public string coBorrowerSocialSecurityNumber { get; set; }
        public string borrowerEmployerName { get; set; }
        public string borrowerJobTitle { get; set; }
        public string borrowerYearsInLineOfWork { get; set; }
        public string borrowerEmployerAddress { get; set; }
        public string borrowerEmployerCityStateZip { get; set; }
        public string borrowerIncome { get; set; }
        public string borrowerSocialSecurityNumber { get; set; }
        public string loanOfficer { get; set; }
        public string losSource { get; set; }
        public string losMilestone { get; set; }
        public string fax { get; set; }
        public string leadSource { get; set; }
        public string homePhone { get; set; }
        public string borrowerMiddleCreditScore { get; set; }
        public string coborrowerMiddleCreditScore { get; set; }
        public string borrowerEmploymentStatus { get; set; }
        public string coborrowerEmploymentStatus { get; set; }
        public string borrowerYearsOnJob { get; set; }
        public string coBorrowerYearsOnJob { get; set; }

        // Constructor
        public TransactionContact() {
            losSource = "Encompass";
        }

        // Tostring
        public override string ToString()
        {
            string result = Utils.parseValue(this.contactId);
            result += Utils.parseValue(this.birthdate);
            result += Utils.parseValue(this.borrowerEmployerCityStateZip);
            result += Utils.parseValue(this.borrowerEmployerName);
            result += Utils.parseValue(this.borrowerIncome);
            result += Utils.parseValue(this.borrowerJobTitle);
            result += Utils.parseValue(this.borrowerSocialSecurityNumber);
            result += Utils.parseValue(this.borrowerYearsInLineOfWork);
            result += Utils.parseValue(this.borrowerEmployerAddress);
            result += Utils.parseValue(this.coBorrowerBirthdate);
            result += Utils.parseValue(this.coBorrowerEmail);
            result += Utils.parseValue(this.coBorrowerEmployerAddress);
            result += Utils.parseValue(this.coBorrowerEmployerCityStateZip);
            result += Utils.parseValue(this.coBorrowerEmployerName);
            result += Utils.parseValue(this.coBorrowerFirstName);
            result += Utils.parseValue(this.coBorrowerIncome);
            result += Utils.parseValue(this.coBorrowerJobTitle);
            result += Utils.parseValue(this.coBorrowerLastName);
            result += Utils.parseValue(this.coBorrowerMobile);
            result += Utils.parseValue(this.coBorrowerSocialSecurityNumber);
            result += Utils.parseValue(this.coBorrowerWorkPhone);
            result += Utils.parseValue(this.coBorrowerYearsInLineOfWork);
            result += Utils.parseValue(this.email);
            result += Utils.parseValue(this.firstName);
            result += Utils.parseValue(this.lastName);
            result += Utils.parseValue(this.mailingCity);
            result += Utils.parseValue(this.mailingState);
            result += Utils.parseValue(this.mailingStreet);
            result += Utils.parseValue(this.mailingZip);
            result += Utils.parseValue(this.maritalStatus);
            result += Utils.parseValue(this.mobile);
         //   result += Utils.parseValue(this.phone);
            result += Utils.parseValue(this.loanOfficer);
            result += Utils.parseValue(this.losSource);
            result += Utils.parseValue(this.losMilestone);
            result += Utils.parseValue(this.fax);
         //   result += Utils.parseValue(this.borrowerJobTitle);
            result += Utils.parseValue(this.leadSource);
            result += Utils.parseValue(this.homePhone);
            result += Utils.parseValue(this.borrowerMiddleCreditScore);
            result += Utils.parseValue(this.coborrowerMiddleCreditScore);
            result += Utils.parseValue(this.borrowerEmploymentStatus);
            result += Utils.parseValue(this.coborrowerEmploymentStatus);
            result += Utils.parseValue(this.borrowerYearsOnJob);
            result += Utils.parseValue(this.coBorrowerYearsOnJob);
            result = Utils.removeLastCharacter(result);

            return result;
        }



        public bool Equals(TransactionContact other)
        {
            if (other == null)
            {
                return false;
            }

            if (this.contactId.Equals(other.contactId, StringComparison.InvariantCultureIgnoreCase))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
