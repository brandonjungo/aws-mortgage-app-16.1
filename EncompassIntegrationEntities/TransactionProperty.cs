﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EncompassIntegrationUtils;

/* 
 * This class defines the bean for the Contact object
 *
 * @author Angelica Buffa
 * @since 2014-10
 */
namespace EncompassIntegrationEntities
{
    public class TransactionProperty
    {
        // Public instance variables
        public string transactionPropertyId { get; set; } 
        public string loanNumber { get; set; }
        public string propertyAddress { get; set; }
        public string propertyCity { get; set; }
        public string propertyState { get; set; }
        public string propertyZip { get; set; }
        public string propertyType { get; set; }
        public string purchasePrice { get; set; }
        public string appraisedValue { get; set; }
        public string loanPurpose { get; set; }
        public string Occupancy { get; set; }
        public string estimatedClosingDate { get; set; }
        public string loanAmount { get; set; }
        public string loanProgram { get; set; }
        public string rate { get; set; }
        public string downPayment { get; set; }
        public string indexPercentage { get; set; }
        public string lender { get; set; }
        public string loanType { get; set; }
        public string lockDate { get; set; }
        public string ltvPercentage { get; set; }
        public string cltvPercentage { get; set; }
        public string marginPercentage { get; set; }
        public string monthlyPayment { get; set; }
        public string closingDate { get; set; }
        public string loanOfficer { get; set; }
        public string borrowerWorkPhone { get; set; }
        public string term { get; set; }
        public string firstPaymentDate { get; set; }
        public string propertyTax { get; set; }
        public string harzadsIns { get; set; }
        public string mortgageIns { get; set; }
        public string hoa { get; set; }
        public string loanProcessor { get; set; }
        public string name { get; set; }
        public string borrowerFirstName { get; set; }
        public string borrowerLastName { get; set; }
        public string borrowerId { get; set; }
        public string borrowerBirthday { get; set; }
        public string losSource { get; set; }
        public string losMilestone { get; set; }
        public string realtorListingAgentName { get; set; }
        public string realtorBuyingAgentName { get; set; }
        public string fileOpenDate { get; set; }
        public string loanApplicationDate { get; set; }
        public String lockExpirationDate { get; set; }
        public string buyerAttorneyName { get; set; }
        public string sellerAttoneyName { get; set; }

        // Constructor
        public TransactionProperty()
        {
            this.losSource = "Encompass";
        }

        // Tostring
        public override string ToString()
        {       
            string result = "";
            result += Utils.parseValue(this.appraisedValue);
            result += Utils.parseValue(this.closingDate);
            result += Utils.parseValue(this.cltvPercentage);
            result += Utils.parseValue(this.downPayment);
            result += Utils.parseValue(this.estimatedClosingDate);
            result += Utils.parseValue(this.firstPaymentDate);
            result += Utils.parseValue(this.harzadsIns);
            result += Utils.parseValue(this.hoa);
            result += Utils.parseValue(this.indexPercentage);
            result += Utils.parseValue(this.loanNumber);
            result += Utils.parseValue(this.lender);
            result += Utils.parseValue(this.loanAmount);
            result += Utils.parseValue(this.loanProcessor);
            result += Utils.parseValue(this.loanProgram);
            result += Utils.parseValue(this.loanPurpose);
            result += Utils.parseValue(this.loanType);
            result += Utils.parseValue(this.lockDate);
            result += Utils.parseValue(this.ltvPercentage);
            result += Utils.parseValue(this.marginPercentage);
            result += Utils.parseValue(this.monthlyPayment);
            result += Utils.parseValue(this.mortgageIns);
            result += Utils.parseValue(this.Occupancy);
            result += Utils.parseValue(this.propertyAddress);
            result += Utils.parseValue(this.propertyCity);
            result += Utils.parseValue(this.propertyState);
            result += Utils.parseValue(this.propertyTax);
            result += Utils.parseValue(this.propertyType);
            result += Utils.parseValue(this.propertyZip);
            result += Utils.parseValue(this.purchasePrice);
            result += Utils.parseValue(this.rate);
            result += Utils.parseValue(this.term);
            result += Utils.parseValue(this.transactionPropertyId);
            result += Utils.parseValue(this.name);
            result += Utils.parseValue(this.borrowerId);
            result += Utils.parseValue(this.loanOfficer);
            result += Utils.parseValue(this.losSource);
            result += Utils.parseValue(this.losMilestone);
            result += Utils.parseValue(this.fileOpenDate);
            result += Utils.parseValue(this.loanApplicationDate);
            result += Utils.parseValue(this.lockExpirationDate);
            result += Utils.parseValue(this.realtorBuyingAgentName);
            result += Utils.parseValue(this.realtorListingAgentName);
            result += Utils.parseValue(this.sellerAttoneyName);
            result += Utils.parseValue(this.buyerAttorneyName);
            result += Utils.parseValue(this.losstatus);
            result = Utils.removeLastCharacter(result);

            return result;
        }

        public string losstatus { get; set; }
    }
}
