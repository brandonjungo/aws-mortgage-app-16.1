﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EncompassIntegrationUtils;
/* 
 * This class defines the structure of Folder bean
 *
 * @author Angelica Buffa
 * @since 2014-10
 */
namespace EncompassIntegrationEntities
{
    [DataContract(Namespace = Constants.Namespace)]
    public class Folder
    {
        [DataMember]
        public string folderName;
        [DataMember]
        public string folderLabel;
    }
}
