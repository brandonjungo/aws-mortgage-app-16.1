﻿using EncompassIntegrationUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

/* 
 * This class defines the structure of the response returned by the WS
 *
 * @author Angelica Buffa
 * @since 2014-10
 */
namespace EncompassIntegrationEntities
{
    [DataContract(Namespace = Constants.Namespace)]
    public class OperationResult
    {
        // Public instance variables
        [DataMember]
        public string error { get; set; }
        [DataMember]
        public Boolean success { get; set; }

        // Constructor
        public OperationResult()
        {
            error = "";
            success = true;
        }
    }
}
