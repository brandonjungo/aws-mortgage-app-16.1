﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EncompassIntegrationUtils;

/* 
 * This class defines the structure of the result returned by the Webservice when returning the folder names
 *
 * @author Angelica Buffa
 * @since 2014-10
 */
namespace EncompassIntegrationEntities
{
    [DataContract(Namespace = Constants.Namespace)]
    [KnownType(typeof(Folder))]
    public class GetFolderResult
    {
        [DataMember]
        public string error { get; set; }
        [DataMember]
        public List<Folder> folders { get; set; }

        public GetFolderResult()
        {
            error = "";
            folders = new List<Folder>();
        }
    }
}
