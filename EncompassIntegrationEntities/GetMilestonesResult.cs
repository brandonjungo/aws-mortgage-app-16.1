﻿using EncompassIntegrationUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

/* 
 * This class defines the structure of the result returned by the Webservice when returning the milestone names
 *
 * @author Angelica Buffa
 * @since 2014-10
 */
namespace EncompassIntegrationEntities
{
    [DataContract(Namespace = Constants.Namespace)]
    public class GetMilestonesResult
    {
        [DataMember]
        public string error { get; set; }
        [DataMember]
        public List<string> milestones { get; set; }

        public GetMilestonesResult()
        {
            error = "";
            milestones = new List<string>();
        }

        public void addMilestone(string milestone)
        {
            this.milestones.Add(milestone);
        }
    }
}
