﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EncompassIntegrationUtils;

/* 
 * This class defines the structure of the response returned by the WS
 *
 * @author Angelica Buffa
 * @since 2014-10
 */
namespace EncompassIntegrationEntities
{
    [DataContract(Namespace = Constants.Namespace)]
    public class GetImportDataResult
    {
        // Public instance variables
        [DataMember]
        public string error { get; set; }
        [DataMember]
        public string jobId { get; set; }

        // Constructor
        public GetImportDataResult()
        {
            error = "";
            jobId = "";
        }
    }

    
   
}
